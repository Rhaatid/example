<div class="purchase-container">
  <div class="purchase-container__text">
  <?
    Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
    "data_title" => $data_text_title,
    "data_text" => $data_text_content,
    "step" => $data_step
    ));
  ?>
  </div>
  <div class="purchase-container__block">
    <div class="purchase-container__block-ball">
      <div class="purchase-container__block-ball-title">
        For example:
      </div>
      
      <div class="purchase-container__block-ball-card-image"><?php print $data_image_link ?></div>
      <div class="purchase-container__block-ball-card-attribute"><?php print $data_attribute?></div>
    </div>
    <div class="purchase-container__block-cart"></div>
  </div>
</div>
