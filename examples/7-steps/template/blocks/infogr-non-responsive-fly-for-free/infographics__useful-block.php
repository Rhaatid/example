<div class="useful-container">
  <div class="useful-container__title"><h3><?php print $data_title?></h3></div>
  <?
    #найдём наименьший массив и выведем ссылки/иконки, ориентируясь на его длинну 
    $min_array = min($data_links_title, $data_links_id, $data_links_icon);

    for ($n = 0; $n < count($min_array); $n++) 
    {
      ?>
        <div class="useful-container__block">
          <div class="useful-container__block-icon useful-container__block-icon_<?print $data_links_icon[$n]?>"></div>
          <?
            $page_information = \Registry::get("Models\Page")->getPageRowsByID(array('page_id' => $data_links_id[$n]));
            ?>
              <a class="useful-container__block-description" href="<?php print SITE_ROOT . $page_information[0]['page_url']?>"><?=$data_links_title[$n]?></a>
            <?
          ?>
        </div>
      <?
    }
  ?>
</div>
