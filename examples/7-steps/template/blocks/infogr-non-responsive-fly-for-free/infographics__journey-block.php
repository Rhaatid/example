<div class="journey-container">
  <div class="journey-container__content">
    <div class="journey-container__content-block-text">
    <?
      Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
      "data_title"        => $data_text_title,
      "data_text"         => $data_text_content,
      "step"              => $data_step,
      ));
    ?>
    </div>
    <div class="journey-container__content-block-card">
      <div class="journey-container__content-block-card-pic"><?php print $data_product_image?></div>
      <div class="journey-container__content-block-card-apply"><?php print $data_product_apply?></div>
    </div>
  </div>
</div>
