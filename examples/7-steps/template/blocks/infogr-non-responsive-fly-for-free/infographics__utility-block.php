<div class="utility-container">
  <div class="utility-container__block-text">
  <?
    Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
    "data_title" => $data_text_title,
    "data_text" => $data_text_content,
    "step" => $data_step
    ));
  ?>
  </div>
  <div class="utility-container__block-tip">
    <div class="utility-container__block-tip-container">
      <b>Tip:</b> with <?php print $data_product_name?> you won't need to wait until you will be able to redeem your miles.
      <p></p>
      You can redeem miles starting at <span>1 mile</span>.
    </div>
  </div>
  <div class="utility-container__block-reference">
    <div class="utility-container__block-reference-title">
      <h2>Did you know?</h2>
    </div>
    <div class="utility-container__block-reference-container">
      <div class="utility-container__block-reference-container-pic">
        <div class="utility-container__block-reference-container-pic_KLM"></div>
      </div>
      <div class="utility-container__block-reference-container-description">KLM is the worlds' oldest airline established in 1919.</div>
    </div>              
    <div class="utility-container__block-reference-container">
      <div class="utility-container__block-reference-container-pic">
        <div class="utility-container__block-reference-container-pic_american"></div>
      </div>
      <div class="utility-container__block-reference-container-description">American Airlines saved $40,000 in 1987 by removing 1 olive from each salad served in first class.</div>
    </div>             
     <div class="utility-container__block-reference-container">
      <div class="utility-container__block-reference-container-pic">
        <div class="utility-container__block-reference-container-pic_alaska"></div>
      </div>
      <div class="utility-container__block-reference-container-description">The Internet/On-Line check-in was first used by Alaska Airlines in 1999.</div>
    </div>
  </div>
</div>
