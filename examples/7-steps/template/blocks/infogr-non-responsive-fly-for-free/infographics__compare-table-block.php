<div class="compare-table-container">
  <div class="compare-table-container__block-ticket">
    <div class="compare-table-container__block-ticket-title">
      <div class="compare-table-container__block-ticket-title-icon"></div>
    </div>
    <div class="compare-table-container__block-ticket-body">
      <div class="compare-table-container__block-ticket-body-description">
        <div class="compare-table-container__block-ticket-body-description-content">Say you earn  <span>50,000</span>  miles by the end of your first year. </div>
        <div class="compare-table-container__block-ticket-body-description-content">Discover will match them so you'll have <span>100,000</span> miles. </div>
      </div>

      <div class="compare-table-container__block-ticket-body-tear-off">
		Enough for a round-trip ticket to London**
      </div>
    </div>
  </div>
  <div class="compare-table-container__block-text">
    <?
      Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
      "data_title" => $data_text_title,
      "data_text" => $data_text_content,
      "step" => $data_step
      ));
    ?>
  </div>
  <div class="compare-table-container__block-table">
    <div class="compare-table-container__block-table-title"><h2>So, lets compare most popular credit cards in terms of earning miles</h2></div>
    <div class="compare-table-container__block-table-body">
      <div class="compare-table-container__block-table-body-attribute">
        <?

        foreach ($data_attributes_titles as $key => $title) 
        {
          ?>
            <div class="compare-table-container__block-table-body-card-attribute compare-table-container__block-table-body-attribute-title attribute-line_<?php print $key?>"><?php print $title; ?></div>
          <?
        }
        
        ?>
      </div>
      <?
      for ($n = 0; $n < count($data_product_array); $n++) 
      {
        $data_product_array_alt = $data_product_array[$n]['cts1']['name'];
        $content__image_src = Registry::get('Controllers\Product')->getImageSourceLink([
            'scale_h'       => 160,
            'scale_v'       => 160,
            'shadow'        => 0,
            'rotation'      => 0,
            'product_id'    => $data_product_array[$n]['cts1']['number'],
            'path'          => $data_product_array[$n]['mnemonics']['image']['value']
        ]);
        $content__image = "<img itemprop=\"image\" src=\"" . $content__image_src . "\" alt=\"$data_product_array_alt\" />";

        $card_image_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
            'scope' => 'page',
            'scheme' => $data_product_array[$n]['cts1']['attributes']['10153'],
            'requested_scheme' => 'pic',

            'page_id'         => $page_no,
            'customer_id'     => $data_product_array[$n]['cts2']['merchant'],
            'product_id'      => $data_product_array[$n]['cts1']['number'],
            'product_href'    => $data_product_array[$n]['cts1']['page']['url'],
            'product_content' => $content__image,
        ));

        $product_name = Registry::get('Controllers\ProductAttribute')->removeSupElementFromName(
            $data_product_array[$n]['cts2']['name']
        );
        $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
            'scope' => 'page',
            'scheme' => $data_product_array[$n]['cts1']['attributes']['attributes']['10153'],
            'requested_scheme' => 'cardname',

            'page_id'         => $page_no,
            'customer_id'     => $data_product_array[$n]['cts2']['merchant'],
            'product_id'      => $data_product_array[$n]['cts1']['number'],
            'product_href'    => $data_product_array[$n]['cts1']['page']['url'],
            'product_content' => $product_name,
            'product_spreadsheet'   => $data_product_array[$n]['cts1']['spreadsheet_number']
        ));
        
        $cards_count = count($data_product_array);

        ?>

        <div class="compare-table-container__block-table-body-card compare-table-container__block-table-body-card_block_<?php print $cards_count?> " itemscope itemtype="http://schema.org/Product">
          <div class="compare-table-container__block-table-body-card-name" itemprop="name"><?php print $product_name_link ?></div>
          <div class="compare-table-container__block-table-body-card-pic" itemprop="image"><?php print $card_image_link?></div>
          <?

          foreach ($data_attributes as $key => $attribute) 
          {
            ?>
              <div class="compare-table-container__block-table-body-card-attribute attribute-line_<?php print $key?>" itemprop="description"><?php print $data_product_array[$n]['cts2']['attributes'][$attribute];?></div>
            <?
          }

          ?>
        </div>

        <?
      }
      ?>
    </div>
  </div>
</div>
