<div class="title-container">
  <div class="title-container__intro">
    <div class="title-container__intro-text title-container__intro-text_number"><?php print $data_banner_steps?></div>
    <h2><?php print $data_banner_header?></h2>
    <br/>
    <div class="title-container__intro-text title-container__intro-text_lemon"><?php print $data_banner_text?></div>
  </div>
</div>
<div class="compare-container">
  <div class="compare-container__card compare-container__card_color_dark-blue popup-element-0">
    <div class="compare-container__card-title compare-container__card-title_color_dark-blue">General Travel Card</div>
    <div class="compare-container__card-description compare-container__card-description_plus">Cheap</div>
    <div class="compare-container__card-description compare-container__card-description_plus">No airlines restrictions</div>
    <div class="compare-container__card-description compare-container__card-description_minus">No priority boarding</div>
    <div class="compare-container__card-description compare-container__card-description_minus">No free checked bag</div>
  </div>
  <div class="compare-container__card-versus">VS</div>
  <div class="compare-container__card compare-container__card_color_white-blue popup-element-1">
    <div class="compare-container__card-title compare-container__card-title_color_white-blue">Airline Co-Branded Card</div>
    <div class="compare-container__card-description compare-container__card-description_plus">Travel benefits</div>
    <div class="compare-container__card-description compare-container__card-description_plus">Companion ticket</div>
    <div class="compare-container__card-description compare-container__card-description_minus">Hefty fees</div>
    <div class="compare-container__card-description compare-container__card-description_minus">Redemption restrictions</div>
  </div>
  <div class="compare-container__text">
  <?
    Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
    "data_title" => $data_text_title,
    "step" => $data_step
    ));
  ?>
  </div>
</div>