<div class="travel-container">
  <div class="travel-container__block-text travel-container__block-text_m-l_50 travel-container__block-text_p-t_90">
    <?
      Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
      "data_title" => $data_text_title,
      "data_text" => $data_text_content,
      "step" => $data_step
      ));
    ?>
  </div>
  <div class="travel-container__block-airline"></div> 
  <div class="travel-container__block-text">
    <?
      Loader::get( BLOCKS_PATH . 'infographics__text-block.php', array( 
      "data_title"        => $data_text_title_anotherOne,
      "data_text"         => $data_text_content_anotherOne,
      "step"              => $data_step_anotherOne,
      "data_list_title"   => $data_step_list_title_anotherOne,
      "data_list_content" => $data_step_list_content_anotherOne
      ));
    ?>
  </div>
  <div class="travel-container__table">
    <div class="travel-container__table-title">
      <h2>Did you know?</h2>
    </div> 
    <div class="travel-container__table-body">
      <div class="travel-container__table-body-sub">
        <div class="travel-container__table-body-sub-title"><?php print $data_domestic_title ?> <?php print $data_product_name?></div>
        <div class="travel-container__table-body-sub-container">
          <div class="travel-container__table-body-sub-container-block">
            <div class="travel-container__table-body-sub-container-block-top"><?php print $data_domestic_first['price']?></div>
            <?
              foreach ($data_domestic_first['destinations'] as $destination) 
              {
                ?>
                  <div class="travel-container__table-body-sub-container-block-bottom travel-container__table-body-sub-container-block-bottom_<?php print $destination?>"></div>
                <?
              }
            ?>
          </div> 
          <div class="travel-container__table-body-sub-container-block">
            <div class="travel-container__table-body-sub-container-block-top"><?php print $data_domestic_second['price']?></div>
            <?
              foreach ($data_domestic_second['destinations'] as $destination) 
              {
                ?>
                  <div class="travel-container__table-body-sub-container-block-bottom travel-container__table-body-sub-container-block-bottom_<?php print $destination?>"></div>
                <?
              }
            ?>
          </div> 
        </div>
      </div>       
      <div class="travel-container__table-body-sub">
        <div class="travel-container__table-body-sub-title"><?php print $data_international_title ?> <?php print $data_product_name?></div>
        <div class="travel-container__table-body-sub-container">
          <div class="travel-container__table-body-sub-container-block">
            <div class="travel-container__table-body-sub-container-block-top"><?php print $data_international_first['price']?></div>
            <?
              foreach ($data_international_first['destinations'] as $destination) 
              {
                ?>
                  <div class="travel-container__table-body-sub-container-block-bottom travel-container__table-body-sub-container-block-bottom_<?php print $destination?>"></div>
                <?
              }
            ?>
          </div> 
          <div class="travel-container__table-body-sub-container-block">
            <div class="travel-container__table-body-sub-container-block-top"><?php print $data_international_second['price']?></div>
            <?
              foreach ($data_international_second['destinations'] as $destination) 
              {
                ?>
                  <div class="travel-container__table-body-sub-container-block-bottom travel-container__table-body-sub-container-block-bottom_<?php print $destination?>"></div>
                <?
              }
            ?>
          </div> 
        </div>
      </div> 
    </div>
  </div>
</div> 
