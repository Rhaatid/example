<div class="infographics-step infographics-step__number_<?=$step?>"></div>
<div class="infographics-step__title"><h3><?php print $data_title?></h3></div>
  <?
    if(!empty($data_text)) 
    {
      ?>
        <div class="infographics-step__text"><?php print $data_text;?></div>
      <?
    }
  ?>
<div class="infographics-step__list">
  <?
    if(!empty($data_list_content)) 
    {
      ?>

      <p><?php print $data_list_title ?></p>
      <ul class="infographics-step__list-container">
        <?
        foreach ($data_list_content as $key => $value) 
        {
          ?>
            <li><?php print $value?></li> 
          <?
        }
        ?>
      </ul>

      <?
    }
  ?>
</div>
