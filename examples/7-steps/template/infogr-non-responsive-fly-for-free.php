<?
use Models\Objects\WebFileObject;
use Controllers\Loaders\WebStyleFileObjectLoader;
$Mustache = new Mustache_Engine;
$products = Registry::get("Models\Product")->getDataUsingPageId(array(
                "page_number" => ($secondOrder && $fromPage>0)? $fromPage : $page_no,
                "structure"   => "cts1&cts2"
            ));
$cts_parameters = Registry::get("Helpers\Formatter")->formatParametrsCTS2([
                    'array'             => $page_data['template_parameters'], 
                    'inserted_is_array' => 1
                  ]);
#костыль для попапа
define ("NO_POPUP", true);
#костыль для страницы, открываемой на мобильном. эта срока нужна только если страница неадаптивна!
define ("NO_ADAPTIVE", true);

# echo "<xmp>"; print_r($cts_parameters); print_r($products); print_r($page_data['template_parameters']); echo "</xmp>";
?>
<? require (BLOCKS_PATH . 'head.php'); ?>

<!--[if lt IE 8]> <html lang="en" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="lt-ie9">        <![endif]-->
<!--[if gt IE 8]> <html lang="en">                       <![endif]-->

<?

$WebStyleFileLoader = new WebStyleFileObjectLoader([
  'root_path_list' => [
    [
      'internal_root_path' => I_ROOT_PATH . 'build/css/',
      'external_root_path' => E_ROOT_PATH . 'build/css/'
    ]
  ]
]);
$WebStyleFileLoader->load([
    new WebFileObject('header.css'),
    new WebFileObject('template__infographics-previous-load.css'),
    new WebFileObject('layout-unadaptive.css')
  ]);
$options = [];

if ( DEBUG ) {
  $options = ['method' => 'attached'];
}

$WebStyleFileLoader->render($options);

?>

</head>
<body itemscope itemtype="http://schema.org/WebPage" data-secondorder="<?=$secondOrder?>" data-frompage="<?=$fromPage?>">

  <div class="page">
    <? include BLOCKS_PATH . "base/header.php";  ?>
    <div class="container infographics-grid">
      <?# контентная часть #?>
      <div class="infographics-grid__container">
      <?# блок заголовка и 2х карточек сравения #?>
        <?
          Loader::get( BLOCKS_PATH . 'infographics__intro-compare-block.php', array(
            "data_text_title"    => $cts_parameters['steps']['step_1']['h3_step_1'][0],
            "data_step"          => $cts_parameters['steps']['step_1']['step_number'][0],
            "data_banner_header" => $cts_parameters['headings']['h2'][0],
            "data_banner_steps"  => $cts_parameters['headings']['count_steps'][0],
            "data_banner_text"   => $cts_parameters['headings']['after_title'][0]
          ));
        ?>
      <?# блок заголовка и 2х карточек сравения #?>
      </div>
      <div class="infographics-grid__container">
      <?# контейнер с картой в шаре #?>
        <?
          foreach ($products[1] as $product) 
          {
            $product_alt = $product['cts2']['name'];
            $content__image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                'scale_h'       => 160,
                'scale_v'       => 160,
                'shadow'        => 0,
                'rotation'      => 0,
                'product_id'    => $product['cts2']['number'],
                'path'          => $product['mnemonics']['image']['value']
            ]);

            $content__image = "<img itemprop=\"image\" src=\"" . $content__image_src . "\" alt=\"$product_alt\" />";

            $card_image_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
                'scope'            => 'page',
                'scheme'           => $product['cts1']['attributes']['10153'],
                'requested_scheme' => 'pic',

                'page_id'          => $page_no,
                'customer_id'      => $product['cts2']['merchant'],
                'product_id'       => $product['cts1']['number'],
                'product_href'     => $product['cts2']['PRODUCT_PAGE_URL'],
                'product_content'  => $content__image,
            ));

            $attribute = $cts_parameters['steps']['step_2']['attributes_card_step_2'][0];

            Loader::get( BLOCKS_PATH . 'infographics__purchase-block.php', array( 
              "data_text_title"   => $cts_parameters['steps']['step_2']['h3_step_2'][0],
              "data_text_content" => $cts_parameters['steps']['step_2']['text_step_2'][0],
              "data_step"         => $cts_parameters['steps']['step_2']['step_number'][0],
              "data_image_link"   => $card_image_link,
              "data_attribute"    => $product['cts2']['attributes'][$attribute],
            ));
          }
        ?>
      <?# контейнер с картой в шаре #?>
      </div>
      <div class="infographics-grid__container">
      <?# контейнер билета и таблицы сравнения карт #?>
        <?
          $producs_array_int_keys = array_values( $products[2] );

          #обработка текста, в котором пристутствует атрибут продукта
          $text_after_render = Registry::get('Helpers\MustacheRender')->renderParameterAsProductText(
                                [
                                  'template'                 => $cts_parameters["steps"]["step_3"]["text_step_3"][0], 
                                  'context_product'          => $products[1][0],
                                  'context_page_spreadsheet' => $products
                                ]
                               );

      $text_after_render = nl2br($text_after_render);

          Loader::get( BLOCKS_PATH . 'infographics__compare-table-block.php', array(
            "data_text_title"        => $cts_parameters['steps']['step_3']['h3_step_3'][0],
            "data_text_content"      => $text_after_render,
            "data_step"              => $cts_parameters['steps']['step_3']['step_number'][0],
            "data_product_array"     => $producs_array_int_keys,
            "data_attributes"        => $cts_parameters['steps']['step_3']['compare_cards']['attributes_card_step_3'],
            "data_attributes_titles" => $cts_parameters['steps']['step_3']['compare_cards']['title_for_attributes']
          ));
        ?>
      <?# контейнер билета и таблицы сравнения карт #?>
      </div>
      <div class="infographics-grid__container">
      <?# контейнер с подсказкой #?>
        <?

          $utility_product = array_values($products[3]);
          $utility_product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
              'scope'                 => 'page',
              'scheme'                => $utility_product[0]['cts1']['attributes']['10153'],
              'requested_scheme'      => 'cardname',

              'page_id'               => $page_no,
              'customer_id'           => $utility_product[0]['cts2']['merchant'],
              'product_id'            => $utility_product[0]['cts1']['number'],
              'product_href'          => $utility_product[0]['cts2']['PRODUCT_PAGE_URL'],
              'product_content'       => $utility_product[0]["cts2"]["name"],
              'product_spreadsheet'   => $utility_product[0]['cts1']['spreadsheet_number']
          ));

          Loader::get( BLOCKS_PATH . 'infographics__utility-block.php', [
            "data_text_title"   => $cts_parameters['steps']['step_4']['h3_step_4'][0],
            "data_text_content" => $cts_parameters['steps']['step_4']['text_step_4'][0],
            "data_step"         => $cts_parameters['steps']['step_4']['step_number'][0],
            "data_product_name" => $utility_product_name_link
          ]);
        ?>
      <?# контейнер с подсказкой #?>
      </div>
      <div class="infographics-grid__container">
      <?# контейнер с информацией для путешествий #?>
        <?
          #формируем массивы с пунктами назначений местных и международных авиалиний + цены на перелёты
          $domestic = [
            #первый и второй (левый и правый) блоки внутренних авиалиний
            $destination_domestic_first = [
              'price'        => $cts_parameters['steps']['step_6']['did_you_know']['destination_domestic']['domestic_1']['price'][0],
              'destinations' => $cts_parameters['steps']['step_6']['did_you_know']['destination_domestic']['domestic_1']['destinations']
            ],         

            $destination_domestic_second = [
              'price'        => $cts_parameters['steps']['step_6']['did_you_know']['destination_domestic']['domestic_2']['price'][0],
              'destinations' => $cts_parameters['steps']['step_6']['did_you_know']['destination_domestic']['domestic_2']['destinations']
            ]          
          ];

          $international = [
            #первый и второй (левый и правый) блоки международных авиалиний
            $destination_international_first = [
              'price'        => $cts_parameters['steps']['step_6']['did_you_know']['destination_international']['international_1']['price'][0],
              'destinations' => $cts_parameters['steps']['step_6']['did_you_know']['destination_international']['international_1']['destinations']
            ],          

            $destination_international_second = [
              'price'        => $cts_parameters['steps']['step_6']['did_you_know']['destination_international']['international_2']['price'][0],
              'destinations' => $cts_parameters['steps']['step_6']['did_you_know']['destination_international']['international_2']['destinations']
            ]
          ];

          #будущее количество строк в таблице перелётов
          $count_domestic_international = count($domestic_international=[$domestic, $international]);

          $travel_product = array_values($products[3]);
          $travel_product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
              'scope'               => 'page',
              'scheme'              => $travel_product[0]['cts1']['attributes']['10153'],
              'requested_scheme'    => 'cardname',

              'page_id'             => $page_no,
              'customer_id'         => $travel_product[0]['cts2']['merchant'],
              'product_id'          => $travel_product[0]['cts1']['number'],
              'product_href'        => $travel_product[0]['cts2']['PRODUCT_PAGE_URL'],
              'product_content'     => $travel_product[0]["cts2"]["name"],
              'product_spreadsheet' => $travel_product[0]['cts1']['spreadsheet_number']
          ));

          #обработка текста, в котором пристутствует атрибут продукта
          $text_after_render = Registry::get('Helpers\MustacheRender')->renderParameterAsProductText(array(
                        'template'                 => $cts_parameters["steps"]["step_5"]["text_step_5"][0], 
                        'context_product'          => $products[1][0],
                        'context_page_spreadsheet' => $products
                      ));

          Loader::get( BLOCKS_PATH . 'infographics__travel-block.php', array(
            "data_text_title"                   => $cts_parameters['steps']['step_5']['h3_step_5'][0],
            "data_text_content"                 => $text_after_render,
            "data_step"                         => $cts_parameters['steps']['step_5']['step_number'][0],
            "data_text_title_anotherOne"        => $cts_parameters['steps']['step_6']['h3_step_6'][0],
            "data_text_content_anotherOne"      => $cts_parameters['steps']['step_6']['text_step_6'][0],
            "data_step_anotherOne"              => $cts_parameters['steps']['step_6']['step_number'][0],
            "data_step_list_title_anotherOne"   => $cts_parameters['steps']['step_6']['step_list_title'][0],
            "data_step_list_content_anotherOne" => $cts_parameters['steps']['step_6']['step_list_content'],
            "data_count_domestic_international" => $count_domestic_international,
            "data_domestic_title"               => $cts_parameters['steps']['step_6']['did_you_know']['destination_domestic']['title'][0],
            "data_domestic_first"               => $destination_domestic_first,
            "data_domestic_second"              => $destination_domestic_second,
            "data_international_title"          => $cts_parameters['steps']['step_6']['did_you_know']['destination_international']['title'][0],
            "data_international_first"          => $destination_international_first,
            "data_international_second"         => $destination_international_second,
            "data_product_name"                 => $travel_product_name_link
          ));
        ?>
      <?# контейнер с информацией для путешествий #?>
      </div>
      <div class="infographics-grid__container">
      <?# блок поездки #?>
        <?
          #получаем картинку продукта, линк по схеме и кнопку апплая
          $journey_product = array_values($products[3]);
          $journey_product_alt = $journey_product[0]['cts2']['name'];
          $journey__image_src = Registry::get('Controllers\Product')->getImageSourceLink([
            'scale_h'       => 253,
            'scale_v'       => 253,
            'shadow'        => 0,
            'rotation'      => 0,
            'product_id'    => $journey_product[0]['cts2']['number'],
            'path'          => $journey_product[0]['mnemonics']['image']['value']
          ]);
          
          $journey_image = "<img itemprop=\"image\" src=\"" . $journey__image_src . "\" alt=\"$journey_product_alt\" />";
          $journey_product_image_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
            'scope'            => 'page',
            'scheme'           => $journey_product[0]['cts1']['attributes']['10153'],
            'requested_scheme' => 'pic',

            'page_id'          => $page_no,
            'customer_id'      => $journey_product[0]['cts2']['merchant'],
            'product_id'       => $journey_product[0]['cts1']['number'],
            'product_href'     => $journey_product[0]['cts1']['prd_href'],
            'product_href'     => $journey_product[0]['cts2']['PRODUCT_PAGE_URL'],
            'product_content'  => $journey_image,
          ));

          $journey_apply = Registry::get('Controllers\Product')->getLinkAsElement(array(
            'scope'                 => 'page',
            'scheme'                => $journey_product[0]["cts1"]['attributes']['10153'],
            'requested_scheme'      => 'button',

            'page_id'               => $page_no,
            'customer_id'           => $journey_product[0]["cts1"]['customer']['number'],
            'product_id'            => $journey_product[0]["cts1"]['number'],
            'product_href'          => $journey_product[0]['cts2']['PRODUCT_PAGE_URL'],
            'product_spreadsheet'   => $journey_product[0]["cts1"]['spreadsheet_number'],
            'product_content'       => "Apply Online",
            "element" => array(
                "class" => array(
                    "journey-container__content-block-card-button"
                )
            )
          ));

          Loader::get( BLOCKS_PATH . 'infographics__journey-block.php', array( 
            "data_product_apply" => $journey_apply,
            "data_product_image" => $journey_product_image_link,
            "data_text_title"    => $cts_parameters['steps']['step_7']['h3_step_7'][0],
            "data_text_content"  => $cts_parameters['steps']['step_7']['text_step_7'][0],
            "data_step"          => $cts_parameters['steps']['step_7']['step_number'][0],
            ));
        ?>
      <?# блок поездки #?>
      </div>

      <div class="updated"><b>Updated:</b> <?=date("F j, Y", strtotime("yesterday"))?></div>

      <div class="infographics-grid__container">
      <?# блок комментариев #?>
        <div class="container-comments container-comments_infographics-comments">
          <?
            foreach ($cts_parameters['footnotes_for_page'] as $value) 
            {
              ?>
                <p><?php print $value[0] ?></p> 
              <?
            }
          ?>
          <div class="footnote"></div>
        </div>
      <?# блок комментариев #?>
      </div>
      <div class="infographics-grid__container">
      <?# блок расшаривания в соцсетях #?>
        <?
          #нужно завести внутренние параметры для передачи в блок, туда похоже нужно будет параметры из meta передавать
          Loader::get( BLOCKS_PATH . 'common__share-facebook-block.php', array());
        ?>
      <?# блок расшаривания в соцсетях #?>
      </div>
      <div class="infographics-grid__container">
      <?# блок ссылок #?>
        <?
          Loader::get( BLOCKS_PATH . 'infographics__useful-block.php', array( 
            "data_title"       => $cts_parameters['bottom_links']['h4_link'][0],
            "data_links_title" => $cts_parameters['bottom_links']['link_title'],
            "data_links_id"    => $cts_parameters['bottom_links']['page_id'],
            "data_links_icon"  => $cts_parameters['bottom_links']['icons']
          ));
        ?>
      <?# блок ссылок #?>
      </div>
      <div class="infographics-grid__container">
      <?# блок новостей #?>
        <?
          include_once (BLOCKS_PATH . 'common__bottom-news-block.php');
        ?>
      <?# блок новостей #?>
      </div>
      <?# контентная часть #?>
    </div>
  </div>
<script src="<?php print E_BUNDLES_PATH . "0.common.js?cache=" . substr( md5( file_get_contents( I_BUNDLES_PATH . "0.common.js" ) ), 0, 8 ) ?>"></script>
<script src="/build/bundles/menu__unadaptive.js"></script>
<script src="/build/bundles/template__infographics.js?<?=md5(time())?>"></script>
<script src="/build/js/header-footer__functions.js"></script>
<script src="<?php print E_BUNDLES_PATH . "footer-signin.js"?>"></script>
<link rel="stylesheet" href="build/css/footer.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/template__infographics.css?<?=md5(time())?>">
<link type="text/css" href="<?=SITE_ROOT?>build/css/subscribe.css" rel="stylesheet" media="all"/>
  
<? include BLOCKS_PATH . "footer.php"; ?>
<? include CONTENT_PATH . "script-facebook.php"; ?>
</body>
</html>

