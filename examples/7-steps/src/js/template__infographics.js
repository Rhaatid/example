import $ from 'jquery';
import 'jquery.alignheight';

import 'popup_v2';

import FootnoteLoader from 'footnote';

import Statistic from 'statistic';

let footnoteLoader = new FootnoteLoader({
    querySelector: '.footnote',
    includeСamouflaged: true
});

var sectionAlign = function() {
    $(".compare-table-container__block-table-body-card-name").alignHeight();
    $(".utility-container__block-reference-container-pic").alignHeight();
    $(".news-container__artcle-name").alignHeight();
    $(".attribute-line_0").alignHeight();
    $(".attribute-line_1").alignHeight();
}

$(document).ready(() => {
    footnoteLoader.init();

    sectionAlign();

    Statistic.updateScreenResolution();
});
