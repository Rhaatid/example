<?php
use Models\Objects\WebFileObject;
use Controllers\Loaders\WebStyleFileObjectLoader;

//require_once(I_BLOCKS_PATH . "base/parameters.php");

require (I_BLOCKS_PATH . 'head.php');
?>

<!--[if lt IE 8]> <html lang="en" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="lt-ie9">        <![endif]-->
<!--[if gt IE 8]> <html lang="en">                       <![endif]-->

<?

$WebStyleFileLoader = new WebStyleFileObjectLoader([
    'root_path_list' => [
        [
            'internal_root_path' => I_ROOT_PATH . 'build/css/',
            'external_root_path' => E_ROOT_PATH . 'build/css/'
        ]
    ]
]);
$WebStyleFileLoader->load([
//    new WebFileObject('template__splash-gas-calculator-respons.css'),
    new WebFileObject('template__splash-gas-calculator-respons-previous-load.css'),
    new WebFileObject('layout.css')
]);
$options = [];

if ( DEBUG ) {
    $options = ['method' => 'attached'];
}

$WebStyleFileLoader->render($options);

?>

<?
# массив продуктов - $products
$products_data = $products_object->getRawProductDataCTSII([
    'PAGE_NUMBER_LIST' => [$order_page_number]
]);

$products = $formatter_object->formatRawProductDataOptional([
    'products_data' => $products_data,
    'page_number' => $order_page_number,
    'entity'        => [
        'mnemonics', 'types'
    ],
    'indexes' => ['product' => 'SYSTEM_2']
]);

?>

</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div class="page">
    <? include I_BLOCKS_PATH . "base/header.php";  ?>
    <div class="container container_slide gas-calculator-grid">
        <? #  блок навигации хлебными крошками # ?>
        <?
        Loader::get(BLOCKS_PATH . 'common__breadcrumbs_new.php', [
            'data_breadcrumbs' => $page_parameters['breadcrumbs']['breadcrumbs']
        ]);
        ?>
        <? #  блок навигации хлебными крошками # ?>
        <div class="gas-calculator-grid__container">
            <div class="banner">
                <div class="banner__block">
                    <h3><?php print $page_parameters['top_texts_and_headings']['banner_text'][0]['value']?></h3>
                    <div class="banner__text banner__text_desktop">
                        <h2><?php print $page_parameters['top_texts_and_headings']['h2'][0]['value']?></h2>
                        <?php print $page_parameters['top_texts_and_headings']['text'][0]['value']?>
                    </div>
                </div>
                <div class="banner__text banner__text_mobile">
                    <h2><?php print $page_parameters['top_texts_and_headings']['h2'][0]['value']?></h2>
                    <div class="js-collapse-banner-parent">
                        <div class="js-banner-text-collapse">
                            <?php print $page_parameters['top_texts_and_headings']['text'][0]['value']?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="calculator">
                <h3>
                    <?php print $page_parameters['calculator']['calculator_title'][0]['value']?>
                </h3>
            </div>
            <div class="compared" id="calculator_root">
<!--                <div>--><?php //$_products[0]?><!--</div>-->
            </div>

            <? /* средний блок с текстами и двумя картами */ ?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__middle-block.php', [
                'data_content' => $page_parameters,
                'product_list' => $products[$order_page_number][3],
            ]);
            ?>
            <? /* средний блок с текстами и двумя картами */ ?>

            <? /* блок с вопросами  */?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__questions-block.php', [
                'data_content' => $page_parameters['questions'],
                'DBC'          => $DBC
            ]);
            ?>
            <?/* блок с вопросами  */ ?>

            <? /* блок с таблицей карт*/ ?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__bottom-table.php', [
                'data_content' => $page_parameters['credit_cards'],
                'product_list'      => $products[$order_page_number][4],
            ]);
            ?>
            <? /* блок с таблицей карт */ ?>

            <? /* Tips*/ ?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__tips.php', [
                'data_content' => $page_parameters['tips']
            ]);
            ?>
            <? /* Tips */ ?>

            <? /* блок с картой и самолетиками*/ ?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__bottom-card.php', [
                'data_content' => $page_parameters['credit_cards_2'],
                'product_list'      => $products[$order_page_number][5]
            ]);
            ?>
            <? /* блок с картой и самолетиками*/ ?>

            <div class="updated"><b>Updated:</b> <?=date("F j, Y", strtotime("yesterday"))?></div>

            <?// Блок с перелинковками и текстом ?>
            <?
            Loader::get(BLOCKS_PATH . 'category/splashes/splash-gas-calculator-respons/gas-calculator-respons__links-block.php', [
                "data_content"      => $page_parameters['bottom_links']
            ]);?>
            <?// Блок с перелинковками и текстом ?>

            <? # сноски# ?>
            <div class="comment">
                <div class="comment-line"></div>
                <?
                foreach ($page_parameters['footnotes']['footnotes'] as $footnote)
                {
                    print "<p class='ftn'>".$footnote['value']."</p>";
                }
                ?>
                <div class="footnote"></div>
            </div>
            <? # сноски# ?>

        </div>

    </div>
    <div class="font-load">&nbsp;</div>
</div>
<? include I_BLOCKS_PATH . "footer.php"; ?>

<link rel="stylesheet" href="build/svg/svg-style.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/template__splash-gas-calculator-respons.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/footer.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/footer-adaptive.css?<?=md5(time())?>">
<link type="text/css" href="<?=SITE_ROOT?>build/css/subscribe.css" rel="stylesheet" media="all"/>

<script src="<?php print E_BUNDLES_PATH . "0.common.js?cache=" . substr( md5( file_get_contents( I_BUNDLES_PATH . "0.common.js" ) ), 0, 8 ) ?>"></script>
<script src="/build/bundles/template__gas-calculator-respons.js"></script>
<script src="/build/bundles/menu__adaptive.js"></script>
<script type="text/javascript" src="/build/js/header-footer__functions.js"></script>
<script src="<?php print E_BUNDLES_PATH . "footer-signin.js"?>"></script>
<? include CONTENT_PATH . "script-facebook.php"; ?>

</body>
</html>