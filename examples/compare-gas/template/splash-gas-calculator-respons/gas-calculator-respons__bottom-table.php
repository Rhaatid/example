<div class="bottom-table">
    <div class="middle-block__title-bg"></div>
    <div class="bottom-table__center">
        <h3><?=$data_content['title'][0]['value']?></h3>
        <table class="bottom-table__center-table-pc">
            <tr>
                <th></th>
                <?foreach($data_content['columns'] as $v){?>
                    <th><?=$v['value']?></th>
                <?}?>
                <th></th>
            </tr>
            <?foreach($product_list as $product){?>
                <tr itemscope itemtype="http://schema.org/Product">


                    <td class="table-card-img">
                        <? $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
                            'scope'             => 'page',
                            'scheme'            => $product['10153'],
                            'requested_scheme'  => 'cardname',

                            'page_id'              => $order_page_number,
                            'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                            'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                            'product_href'         => $product['PRODUCT_PAGE_URL'],
                            'product_content'      => $product['PRODUCT_NAME'],
                            'product_spreadsheet'  => $product['SPREADSHEET_NUMBER'],

                        ));
                        print $product_name_link;

                        $product_alt = $product['PRODUCT_APPLY_URL']? $product['PRODUCT_NAME']." Application" : $product['PRODUCT_NAME'];

                        $image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                            'scale_h'     => 160,
                            'scale_v'     => 160,
                            'shadow'      => 0,
                            'rotation'    => 0,
                            'product_id'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                            'path'        => $product['MNEMONICS']['image']['value']
                        ]);

                        $image = "<img itemprop=\"image\" src=\"" . $image_src . "\" alt=\"$product_alt\" />";

                        $product_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                            'scope'             => 'page',
                            'scheme'            => $product['10153'],
                            'requested_scheme'  => 'pic',

                            'page_id'              => $order_page_number,
                            'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                            'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                            'product_href'         => $product['PRODUCT_PAGE_URL'],
                            'product_spreadsheet'  =>  $product['SPREADSHEET_NUMBER'],
                            'product_content'      => $image
                        ]);

                        print $product_image_link;
                        ?>
                        <meta itemprop="url" content="<?=$product['PRODUCT_PAGE_URL']?>"/>
                        <meta itemprop="name" content="<? print $product['PRODUCT_NAME']?>">
                    </td>
                    <?foreach($data_content['attributes'] as $v){?>
                    <td><?=echo_php_extended(str_replace('#product_id#',$product['PRODUCT_SYSTEM_1_NUMBER'],$product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$v['value']]['value']))?></td>
                    <?}?>
                    <td class="table-card-apply">
                        <?
                        $apply_button = Registry::get('Controllers\Product')->getLinkAsElement([
                            'scope'             => 'page',
                            'scheme'            => $product['10153'],
                            'requested_scheme'  => 'button',

                            'page_id'                   => $order_page_number,
                            'customer_id'               => $product['MERCHANT_SYSTEM_1_NUMBER'],
                            'product_id'                => $product['PRODUCT_SYSTEM_1_NUMBER'],
                            'product_href'              => $product['PRODUCT_PAGE_URL'],
                            'product_spreadsheet'       => $product['SPREADSHEET_NUMBER'],
                            'product_content'           => "Apply",
                            'product_content_replaced'  => true,
                            "element"             => [
                                "class" => [
                                    "card-table-apply"
                                ]
                            ]
                        ]);
                        if($product['PRODUCT_APPLY_URL']!='')print $apply_button;
                        ?>
                    </td>
                </tr>
            <?}?>
        </table>
        <table class="bottom-table__center-table-mobile">
            <?foreach($product_list as $product){?>
            <tr itemscope itemtype="http://schema.org/Product">

                <td class="table-card-name" colspan="3">
                    <? $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
                        'scope'             => 'page',
                        'scheme'            => $product['10153'],
                        'requested_scheme'  => 'cardname',

                        'page_id'              => $order_page_number,
                        'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                        'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                        'product_href'         => $product['PRODUCT_PAGE_URL'],
                        'product_content'      => $product['PRODUCT_NAME'],
                        'product_spreadsheet'  => $product['SPREADSHEET_NUMBER'],

                    ));
                    print $product_name_link;
                    ?>
                    <meta itemprop="url" content="<?=$product['PRODUCT_PAGE_URL']?>"/>
                    <meta itemprop="name" content="<? print $product['PRODUCT_NAME']?>">
                   <?
                    $product_alt = $product['PRODUCT_APPLY_URL']? $product['PRODUCT_NAME']." Application" : $product['PRODUCT_NAME'];

                    $image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                    'scale_h'     => 90,
                    'scale_v'     => 90,
                    'shadow'      => 0,
                    'rotation'    => 0,
                    'product_id'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                    'path'        => $product['MNEMONICS']['image']['value']
                    ]);

                    $image = "<img src=\"" . $image_src . "\" alt=\"$product_alt\" />";
                    ?>
                    <meta itemprop="image" content='<?=$image?>'/>
                </td>
            </tr>
            <tr>
                <td class="table-card-img" rowspan="<?=count($data_content['attributes'])+1?>">
                 <?
                    $product_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                        'scope'             => 'page',
                        'scheme'            => $product['10153'],
                        'requested_scheme'  => 'pic',

                        'page_id'              => $order_page_number,
                        'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                        'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                        'product_href'         => $product['PRODUCT_PAGE_URL'],
                        'product_spreadsheet'  =>  $product['SPREADSHEET_NUMBER'],
                        'product_content'      => $image
                    ]);

                    print $product_image_link;
                    ?>

                 <?
                 $apply_button = Registry::get('Controllers\Product')->getLinkAsElement([
                     'scope'             => 'page',
                     'scheme'            => $product['10153'],
                     'requested_scheme'  => 'button',

                     'page_id'                   => $order_page_number,
                     'customer_id'               => $product['MERCHANT_SYSTEM_1_NUMBER'],
                     'product_id'                => $product['PRODUCT_SYSTEM_1_NUMBER'],
                     'product_href'              => $product['PRODUCT_PAGE_URL'],
                     'product_spreadsheet'       => $product['SPREADSHEET_NUMBER'],
                     'product_content'           => "Apply",
                     'product_content_replaced'  => true,
                     "element"             => [
                         "class" => [
                             "card-table-apply"
                         ]
                     ]
                 ]);
                 if($product['PRODUCT_APPLY_URL']!='')print $apply_button;
                 ?>
                </td>
            </tr>
                <?foreach($data_content['attributes'] as $k => $v){?>
                    <tr>
                        <td class="table-card-title">
                            <?=$data_content['columns'][$k]['value']?>
                        </td>
                        <td class="table-card-attr">
                            <?=echo_php_extended(str_replace('#product_id#',$product['PRODUCT_SYSTEM_1_NUMBER'],$product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$v['value']]['value']))?>
                        </td>
                    </tr>

                 <?}?>

            <?}?>
        </table>
        <div class="bottom-table__center-text">
            <p><?=$data_content['credit_cards_text'][0]['value']?></p>
        </div>
    </div>
</div>