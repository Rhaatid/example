<div class="tips-block">
    <h3><?=$data_content['title'][0]['value']?></h3>
    <?foreach($data_content['headings'] as $k => $v){?>
        <img src="<?=BUILD_PATH?>images/gas-calculator/<?=$v['extra']?>.png"  alt="<?=$v['value']?>">
        <div class="tips-block__tips">
            <div class="tips-block__title"><h4><?=($k+1).') '.$v['value']?></h4></div>
            <p><?=$data_content['texts'][$k]['value']?></p>
        </div>
        <div class="clear"></div>
    <?}?>

</div>
