<?php
/**
 * @var \Models\Page $page_object
 */

$page_object = \Registry::get("Models\Page");

?>
<div class="links-block">
    <h3><?=$data_content['title'][0]['value']?></h3>
    <div class="links-block__icons">
        <div class="links-block__column">
        <? $data_links_icons = $data_content['icons'];$k=0;
        foreach ($data_links_icons as $icon_block_item)
        {
            ?>
            <div>
                <?
                foreach ($icon_block_item as $element)
                {
                    switch ($element['type']) {
                        case 'Text':
                            $icon = $element['value'];

                            break;
                        case 'Page':
                            $page = $page_object->getPageRowsByID(array('page_id' => $element['value']));
                            $link = str_replace( "index.php",  "", $page[0]['page_url']);

                            break;
                    }

                    if (!empty($element['extra']))
                    {
                        ?>
                        <a href="<?php print SITE_ROOT . $link?>" target="_self">
                            <i class="<?php print $icon?>"></i>
                            <span><?php print $element['extra'];?></span>
                        </a>
                        <?
                    }
                }
                ?>
            </div>
            <?if($k == 1)echo'</div><div class="links-block__column">';?>
       <?$k++; }
        ?>
        </div>
    </div>
</div>