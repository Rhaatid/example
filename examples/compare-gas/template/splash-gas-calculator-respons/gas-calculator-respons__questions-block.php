<?
    foreach($data_content['questions_ids'] as $v)
    {
        $num_faqs[] = $v['value'];
    }
    $num_faqs = implode(',',$num_faqs);
?>
<div class="questions-block">
    <h3><?=$data_content['questions_title'][0]['value']?></h3>

    <?php
    $sql = "SELECT DISTINCT p.ID, p.post_date, p.post_title, p.post_name, p.post_content
               FROM faq_new.wp_posts p
               WHERE p.post_status = 'publish'
               AND p.post_type = 'post'"
               .(strlen($num_faqs)>0 ? " AND p.ID in (".$num_faqs.") " : '')."
                ORDER BY FIELD(p.ID,".$num_faqs.")
                LIMIT ".count($data_content['questions_ids']);
    $resFAQS = $DBC['media']->query($sql);
    $numFAQS = $DBC['media']->num_rows($resFAQS);
    while ($rowFAQS = $DBC['media']->fetch_array($resFAQS)){
        $FAQS['post_title'][] = preg_replace("/[^[:alnum:][:punct:]]/"," ", $rowFAQS['post_title']);
        $FAQS['post_content'][] = strip_tags($rowFAQS['post_content']);
        $FAQS['slug'][] = $rowFAQS['slug'];
        $FAQS['post_name'][] = $rowFAQS['post_name'];
        $FAQS['ID'][] = $rowFAQS['ID'];
        $FAQS['cat_name'][] = $rowFAQS['name'];
    }
    for($i=0;$i<$numFAQS;$i++){
        ?>

    <div class="questions-block__question question-<?=$i?>">
        <a href="<?=$dot?>faqs<?=MEDIASUFFIX?>/<?=$FAQS['post_name'][$i]?>-<?=$FAQS['ID'][$i]?>.html" class="js-question-<?=($i==0 || $i==1?'1':'2')?>">
            <?=trim_text($FAQS['post_title'][$i],170);?>
        </a>
        <p class="js-answer-<?=($i==0 || $i==1?'1':'2')?>"><?=trim_text($FAQS['post_content'][$i],400);?></p>
        <a href="<?=$dot?>faqs<?=MEDIASUFFIX?>/<?=$FAQS['post_name'][$i]?>-<?=$FAQS['ID'][$i]?>.html" class="questions-block__more">Read more</a>
    </div>
    <? if($i==1)echo"<div class='clear'></div>";
    }?>
    <div class='clear'></div>
</div>

<? if($numFAQS >0){?>
<div class="banner-middle">
    <img src="<?=BUILD_PATH?>images/gas-calculator/banner-middle.jpg" alt="">
    <div class="banner-middle__center">
        <div class="banner-middle__center-left">
        <?=$data_content['banner_title'][0]['value']?>
        </div>
        <div class="banner-middle__center-right">
            <a href="/faqs/">MORE QUESTIONS & ANSWERS</a>
            <a href="/faqs/ask/" class="ask">Ask Question </a>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?}?>