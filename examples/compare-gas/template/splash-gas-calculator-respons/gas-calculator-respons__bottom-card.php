<div class="middle-block__title-bg"></div>
<div class="img-bottom">
    <img src="<?=BUILD_PATH?>images/gas-calculator/bottom-bg.jpg"  alt=""  >
    <div class="bottom-card">
        <h3><?=$data_content['title'][0]['value']?></h3>
        <?  foreach($product_list as $product){?>
        <div class="bottom-card__card" itemscope itemtype="http://schema.org/Product">
            <meta itemprop="url" content="<?=$product['PRODUCT_PAGE_URL']?>"/>
            <meta itemprop="name" content="<? print $product['PRODUCT_NAME']?>">
            <div class="bottom-card__card-image">
                <?
                $product_alt = $product['PRODUCT_APPLY_URL']? $product['PRODUCT_NAME']." Application" : $product['PRODUCT_NAME'];
                $image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                    'scale_h'     => 160,
                    'scale_v'     => 160,
                    'shadow'      => 0,
                    'rotation'    => 0,
                    'product_id'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                    'path'        => $product['MNEMONICS']['image']['value']
                ]);

                $image = "<img itemprop=\"image\" src=\"" . $image_src . "\" alt=\"$product_alt\" />";

                $product_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                    'scope'             => 'page',
                    'scheme'            => $product['10153'],
                    'requested_scheme'  => 'pic',

                    'page_id'              => $order_page_number,
                    'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                    'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                    'product_href'         => $product['PRODUCT_PAGE_URL'],
                    'product_spreadsheet'  =>  $product['SPREADSHEET_NUMBER'],
                    'product_content'      => $image
                ]);

                print $product_image_link;

                $apply_button = Registry::get('Controllers\Product')->getLinkAsElement([
                    'scope'             => 'page',
                    'scheme'            => $product['10153'],
                    'requested_scheme'  => 'button',

                    'page_id'                   => $order_page_number,
                    'customer_id'               => $product['MERCHANT_SYSTEM_1_NUMBER'],
                    'product_id'                => $product['PRODUCT_SYSTEM_1_NUMBER'],
                    'product_href'              => $product['PRODUCT_PAGE_URL'],
                    'product_spreadsheet'       => $product['SPREADSHEET_NUMBER'],
                    'product_content'           => "Apply Now",
                    'product_content_replaced'  => true,
                    "element"             => [
                        "class" => [
                            "card-apply",
                        ]
                    ]
                ]);
                if($product['PRODUCT_APPLY_URL']!='')print $apply_button;
                ?>

            </div>

            <div class="bottom-card__card-descr">
                <?php print $product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$data_content['features'][0]['value']]['output']?>
            </div>

            <div class="bottom-card__card-descr__bottom">
                <? foreach($data_content['texts'] as $k => $v){?>
                    <div class="attr-<?=($k+1)?>"><?=$v['value']?></div>
                <?}?>
                <div class="clear"></div>
            </div>

        </div>
        <?}?>
    </div>
</div>