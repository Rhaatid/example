<div class="middle-block">
    <h3><?=$data_content['middle_text_block']['text_1_heading'][0]['value']?></h3>
    <? if(strripos($data_content['middle_text_block']['text_1'][0]['value'],'{{') != false)
    {
        $text_top_block = \Registry::get("Helpers\Formatter")->getLinkInTextWithUrl($params = [
            "string" => $data_content['middle_text_block']['text_1'][0]['value']
        ]);
    } else $text_top_block = $data_content['middle_text_block']['text_1'][0]['value'];
    $content_list = \Registry::get("Helpers\Formatter")->splitTextAsArrayOnLineBreaks($params = [
        "text" => $text_top_block
    ]);
    ?>
    <div class="js-collapse-middle-parent">
        <div class="middle-block__text js-middle-text-collapse">
            <?php print $text_top_block?>
        </div>
    </div>

    <?unset($text_top_block,$content_list,$content_line);?>

    <h3><?=$data_content['middle_text_block']['text_2_heading'][0]['value']?></h3>
    <? if(strripos($data_content['middle_text_block']['text_2'][0]['value'],'{{') != false)
    {
    $text_top_block = \Registry::get("Helpers\Formatter")->getLinkInTextWithUrl($params = [
        "string" => $data_content['middle_text_block']['text_2'][0]['value']
    ]);
    } else $text_top_block = $data_content['middle_text_block']['text_2'][0]['value'];
    $content_list = \Registry::get("Helpers\Formatter")->splitTextAsArrayOnLineBreaks($params = [
        "text" => $text_top_block
    ]);
    ?>

    <div class="middle-block__text">
        <? foreach ($content_list as $content_line)
        {
            echo "<p>" . $content_line . "</p>";
        }
        ?>
    </div>
</div>
<div class="middle-block__title-bg"></div>
<div class="middle-block">
    <?$i=1;
    foreach($product_list as $product){?>
    <div class='js-middle-block-card middle-block__card <?=($i==1?'card-left':'card-right')?>' itemscope itemtype="http://schema.org/Product">
        <meta itemprop="name" content="<? print $product['PRODUCT_NAME']?>">
        <h4 class="js-middle-history-title"><?=$product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$data_content['middle_card_block']['attributes'][0]['value']]['value']?></h4>
        <div class="middle-block__card-name js-card-name">
            <h5>
            <? $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
                'scope'             => 'page',
                'scheme'            => $product['10153'],
                'requested_scheme'  => 'cardname',

                'page_id'              => $order_page_number,
                'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                'product_href'         => $product['PRODUCT_PAGE_URL'],
                'product_content'      => $product['PRODUCT_NAME'],
                'product_spreadsheet'  => $product['SPREADSHEET_NUMBER'],

            ));
            print $product_name_link;
            ?>
            </h5>
        </div>
        <div class="clear"></div>
        <meta itemprop="url" content="<?=$product['PRODUCT_PAGE_URL']?>"/>
        <div class="middle-block__card-image">
            <?
            $product_alt = $product['PRODUCT_APPLY_URL']? $product['PRODUCT_NAME']." Application" : $product['PRODUCT_NAME'];

            $image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                'scale_h'     => 262,
                'scale_v'     => 262,
                'shadow'      => 0,
                'rotation'    => 0,
                'product_id'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                'path'        => $product['MNEMONICS']['image']['value']
            ]);

            $image = "<img itemprop=\"image\" src=\"" . $image_src . "\" alt=\"$product_alt\" />";

            $product_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                'scope'             => 'page',
                'scheme'            => $product['10153'],
                'requested_scheme'  => 'pic',

                'page_id'              => $order_page_number,
                'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                'product_href'         => $product['PRODUCT_PAGE_URL'],
                'product_spreadsheet'  =>  $product['SPREADSHEET_NUMBER'],
                'product_content'      => $image
            ]);

            print $product_image_link;
            ?>

        </div>

        <?
        $apply_button = Registry::get('Controllers\Product')->getLinkAsElement([
            'scope'             => 'page',
            'scheme'            => $product['10153'],
            'requested_scheme'  => 'button',

            'page_id'                   => $order_page_number,
            'customer_id'               => $product['MERCHANT_SYSTEM_1_NUMBER'],
            'product_id'                => $product['PRODUCT_SYSTEM_1_NUMBER'],
            'product_href'              => $product['PRODUCT_PAGE_URL'],
            'product_spreadsheet'       => $product['SPREADSHEET_NUMBER'],
            'product_content'           => "Apply Now",
            'product_content_replaced'  => true,
            "element"             => [
                "class" => [
                    "middle-block__card-apply",
                ]
            ]
        ]);
        if($product['PRODUCT_APPLY_URL']!='')print $apply_button;
        ?>

        <div class="middle-block__card-descr js-card-descr" itemprop="description">
            <?=$product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$data_content['middle_card_block']['attributes'][1]['value']]['output']?>
        </div>
    </div>
    <? $i++;}?>
    <div class="clear"></div>
</div>
<?
preg_match_all("|<span>(.*)</span>|U", $data_content['middle_card_block']['credit_history_title'][0]['value'], $match);
$text = $data_content['middle_card_block']['credit_history_title'][0]['value'];
if(count($match)>0)
{
    for($i=0;$i<3;$i++)
    {
        $page_information = \Registry::get("Models\Page")->getPageRowsByID(array('page_id' => $data_content['middle_card_block']['page_ids'][$i]['value']));
        $text = str_replace('<span>'.$match[1][$i].'</span>','<a href="'.SITE_ROOT . $page_information[0]['page_url'].'">'.$match[1][$i].'</a>',$text);

    }
}
?>
<div class="middle-block__bottom"><?=$text?></div>
