import $ from 'jquery';
import 'jquery.alignheight';
import 'tooltip';

// import 'popup_v2';
import popup from 'popup_v2';

import FootnoteLoader from 'footnote';

import Statistic from 'statistic';

let footnoteLoader = new FootnoteLoader({
    querySelector: '.footnote',
    includeСamouflaged: true
});

//для вычисления максимального/минимального значения в массиве (я так захотел)
Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};


let TabAlign = () => {
    let tabItems = $('.js-tab-icon').length;
    let tabContainerWidth =  $('.js-tab-products-container').width();
    $('.js-tab-icon').outerWidth(tabContainerWidth/tabItems);

    // сперва высота выставится инициализированная, после чего высоты будут пушиться в массив,
    // затем выберется максимальная высота и применится на все элементы
    // после очистится массив
    let tabItemsHeight = [];
    $('.js-tab-icon').css('height', 'auto');

    $('.js-tab-icon').each (function() {
        tabItemsHeight.push($(this).outerHeight());
    });

    $('.js-tab-icon').each (function() {
        $(this).outerHeight(tabItemsHeight.max());
    });

    tabItemsHeight= [];
};

let AttributeTabAlign = () => {
    $('.js-attribute-name-selector').alignHeight();
};

let compareWay = () => {
    let cntTables = parseInt($('#count-tables').val());
    if (cntTables==0) cntTables = 5;
    for (var j=1; j<=cntTables; j++) {
        for (var i=1; i<=$('#countCards'+j).val(); i++) {
            $('#compareProduct_'+j+'_'+i).on('click',function(){
    if (parseInt($('#countInCart').val())>0) { document.location.href = '/compare.php'; return false; }
            });
        }
    }
};

$(document).ready(() => {
    footnoteLoader.init();
    TabAlign();
    AttributeTabAlign();
    compareWay();
    Statistic.updateScreenResolution();

    $( window ).on( 'resize', () => {

        clearTimeout( window.resizeTimeout );

        window.resizeTimeout = setTimeout(() => {
            TabAlign();
            AttributeTabAlign();
        }, 500);
    });

    $('.js-tooltip-click').each(function() {
        var tooltipContent = $(this).attr('data-tooltipster-content');
        $(this).tooltipster({
            contentAsHTML: true,
            position: 'top',
            theme: 'tooltipster-cl',
            content: tooltipContent,
            multiple: true,
            interactive: true,
            trigger: 'click',
            functionReady: function () {
                $('.js-tooltip-close').on('click', function() {
                    $('.tooltipster-base').detach();
                });
            }
        });
    });

});
