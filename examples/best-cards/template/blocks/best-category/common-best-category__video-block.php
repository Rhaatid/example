<?php
?>
<div class="common-best__video">
    <div class="common-best__video-title"><?php print $video_title ?></div>
    <div itemscope itemtype="http://schema.org/VideoObject">
        <meta itemprop="description" content="<?php print $video_title ?>">
        <p class="common-best__video-text" itemprop="name"><?php print $video_description ?></p>
        <p class="common-best__video-date" itemprop="uploadDate" content="<?php print $date ?>">
            <?php print $date_formatted?>
        </p>
        <div class="home-video">
            <video class="projekktor" poster="http://<?php print SITE_DOMAIN?>/images/pictures/<?php print $video ?>.jpg" title="<?php print $video ?>" width="304" height="171" controls>
                <source src="http://<?php print SITE_DOMAIN?>/images/video_folder/<?php print $video ?>.mp4" type="video/mp4" />
                <source src="http://<?php print SITE_DOMAIN?>/images/video_folder/<?php print $video ?>.webm" type="video/webm" />
                <source src="http://<?php print SITE_DOMAIN?>/images/video_folder/<?php print $video ?>.ogg" type="video/ogg" />
            </video>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?