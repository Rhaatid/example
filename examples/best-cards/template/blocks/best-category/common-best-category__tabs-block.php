<?php
// из вызвавшего скрипта должны прийти:
// 1 - информация для таба(название класса для иконки и тайтл, заполненный в поле экстра)
// 2 - продукты страницы, для передачи их в скрипт вывода подуктов
// 3 - параметры страницы, для передачи в скрипт вывода подуктов
// 4 - order_page_number для верной записи статы
// 5 - переменная checkedProducts(chProducts) из глобальной области видимости
// 6 - dot
?>
<div class="common-best__tab-block js-tab-products-container">
    <?
    // i - счётчик обозначающий номер таба и номер таблицы с продуктами
    $i= 1;
    foreach ($data_tabs as $element)
    {
        ?>
        <div>
            <input type="radio" class="common-best__tab" name="common-best__tabs" id="tab-<?php print $i?>"  <?php ($i == 1)? print 'checked': '' ?> />
            <label class="js-tab-icon" for="tab-<?php print $i?>">
                <i class="<?php print $element['value'] ?>"></i>
                <span><?php print $element['extra'] ?></span>
            </label>
            <?# вывод продуктов #?>
            <?
            # проверка на номер таблицы для продуктов, если параметр не пустой, то подхватить номер с него,
            # если пуст, то подхватим номер со значения счётчика
            print $data_parameters[$i]['table_number'][0]['value'];
            if (!empty($data_parameters[$i]['table_number'][0]['value']))
            {
                $table_number = $data_parameters[$i]['table_number'][0]['value'];
            } else {
                $table_number = $i;
            }

            Loader::get(BLOCKS_PATH . 'common-best-category__products-block.php', [
                'data_products'      => $data_products[$table_number],
                'data_parameters'    => $data_parameters[$i],
                'order_page_number'  => $order_page_number,
                'checkedProducts'    => $checkedProducts,
                'cntProducts'        => $cntProducts,
                'dot'                => $dot
            ]);
            ?>
            <?# вывод продуктов #?>

            <?// нужно для компейра ?>
            <input type="hidden" name="countCards<?php print $i?>" id="countCards<?php print $i?>" value="<?php print count($data_products[$i]);?>">
        </div>
        <?
        $i++;
    }
    ?>
    <?// нужно для компейра ?>
    <input type="hidden" value="<?=count($data_tabs)?>" id="count-tables" name="count-tables">
</div>
<?