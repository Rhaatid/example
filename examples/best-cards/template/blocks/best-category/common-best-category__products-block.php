<?php
// из вызвавшего скрипта должны прийти 3 параметра
// 1 - продукты 2- параметры 3 - order_page_number
?>
    <div class="common-best__products-block">
        <?
        $printedProducts = 0;
        foreach ($data_products as $product)
        {
            ?>
            <div class="common-best__product" itemtype="http://schema.org/Product">
                <input type="hidden" id="card_<?= $product['SPREADSHEET_NUMBER'] ?>_<?= ($printedProducts + 1) ?>"
                       value="<?= $product['PRODUCT_SYSTEM_1_NUMBER'] ?>">
                <input type="hidden" id="card-type_<?= $product['SPREADSHEET_NUMBER'] ?>_<?= ($printedProducts + 1) ?>"
                       value="<?= $product['PRODUCT_SYSTEM_1_TYPE_NUMBER'] ?>">


                <?# блок с названием карты и кредитной историей #?>
                <div class="common-best__container common-best__container_yellow common-best__product-header">
                    <div class="common-best__product-name">
                        <? $printedProducts == 0 ? print '<div class="common-best__product-choice js-tooltip-click" data-tooltipster-content="<span class=\'js-tooltip-close\'></span>This ranking expresses the Credit-Land.com’s experts’ honest opinions, beliefs, and experiences with credit card products offered at Credit-Land.com. While Credit-Land.com receives compensation from most credit card issuers whose offers appear on the site, this ranking is NOT influenced by payment considerations."></div>':'';?>
                        <?
                        $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement(array(
                            'scope'             => 'page',
                            'scheme'            => $product['PRODUCT_SYSTEM_1_ATTRIBUTE_LIST']['10153']['value'],
                            'requested_scheme'  => 'cardname',

                            'page_id'              => $order_page_number,
                            'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                            'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                            'product_href'         => $product['PRODUCT_PAGE_URL'],
                            'product_content'      => $product['PRODUCT_NAME'],
                            'product_spreadsheet'  => $product['SPREADSHEET_NUMBER'],

                        ));
                        print $product_name_link;
                        ?>

                    </div>
                    <div class="common-best__product-history">
                        <?php print $product['MNEMONICS']['cts2_credit-history']['value']?>
                    </div>
                </div>
                <?# блок с названием карты и кредитной историей #?>

                <div class="common-best__product-content">

                    <?# блок с картинкой карты, рейтингом и кнопкой #?>
                    <div class="common-best__product-view">
                        <div>
                            <?
                            $product_alt = strip_tags($product['PRODUCT_NAME']);
                            $image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                                'scale_h'     => 160,
                                'scale_v'     => 160,
                                'shadow'      => 0,
                                'rotation'    => 0,
                                'product_id'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                                'path'        => $product['MNEMONICS']['image']['value']
                            ]);

                            $image = "<img itemprop=\"image\" src=\"" . $image_src . "\" alt=\"$product_alt\" />";

                            $product_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                                'scope'             => 'page',
                                'scheme'            => $product['PRODUCT_SYSTEM_1_ATTRIBUTE_LIST']['10153']['value'],
                                'requested_scheme'  => 'pic',

                                'page_id'              => $order_page_number,
                                'customer_id'          => $product['MERCHANT_SYSTEM_1_NUMBER'],
                                'product_id'           => $product['PRODUCT_SYSTEM_1_NUMBER'],
                                'product_href'         => $product['PRODUCT_PAGE_URL'],
                                'product_spreadsheet'  =>  $product['SPREADSHEET_NUMBER'],
                                'product_content'      => $image,
                                "element"              => [
                                    "class" => [
                                        "common-best_product-image-container",
                                    ]
                                ]
                            ]);

                            print $product_image_link;
                            ?>

                            <?
                            $cntPrint   = $printedProducts;
                            $chProducts = $checkedProducts;
                            compare_landpage([
                                'data'        => $product,
                                'tableNum'    => $product['SPREADSHEET_NUMBER'],
                                'cntPrint'    => $cntPrint,
                                'cntProducts' => $cntProducts,
                                'chProducts'  => $chProducts,
                                'dot'         => $dot
                            ]);
                            ?>

                            <?
                            Loader::get(TEMPLATES_PATH . 'elements/product-rating.php', [
                                'product_number'  => $product['PRODUCT_SYSTEM_2_NUMBER'],
                                'product_rating'  => $product['MNEMONICS']['cts2_overall-rating']['value'],
                            ]);
                            ?>
                        </div>
                        <div>
                            <div class="common-best__product-content-history">
                                <?php print $product['MNEMONICS']['cts2_credit-history']['value']?>
                            </div>

                            <?
                            $apply_button = Registry::get('Controllers\Product')->getLinkAsElement([
                                'scope'             => 'page',
                                'scheme'            => $product['PRODUCT_SYSTEM_1_ATTRIBUTE_LIST']['10153']['value'],
                                'requested_scheme'  => 'button',

                                'page_id'                   => $order_page_number,
                                'customer_id'               => $product['MERCHANT_SYSTEM_1_NUMBER'],
                                'product_id'                => $product['PRODUCT_SYSTEM_1_NUMBER'],
                                'product_href'              => $product['PRODUCT_PAGE_URL'],
                                'product_spreadsheet'       => $product['SPREADSHEET_NUMBER'],
                                'product_content'           => "Apply <span>Online</span>",
                                'product_content_replaced'  => true,
                                "element"             => [
                                    "class" => [
                                        "common-best__button",
                                        "common-best__button_red"
                                    ]
                                ]
                            ]);

                            print $apply_button;
                            ?>
                        </div>
                    </div>
                    <?# блок с картинкой карты, рейтингом и кнопкой #?>

                    <?# блок с описанием карты #?>
                    <div class="common-best__product-description">
                        <div class="common-best__product-description-title">
                            <?php print $data_parameters['title']['0']['value']?>
                        </div>
                        <?
                        foreach ($data_parameters['description'] as $element)
                        {
                            $attribute = $element['value'];
                            if (count($data_parameters['description']) > 1) {
                                ?>
                                <li><?php  print $product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$attribute]['output'];?></li>
                                <?
                            } else {
                                print $product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$attribute]['output'];
                            }
                        }
                        ?>
                    </div>
                    <?# блок с описанием карты #?>

                    <?# таблица с атрибутами #?>
                    <div class="common-best__product-attributes">
                        <div class="common-best__container">
                            <div class="common-best__container-sibling common-best__table-title common-best__table-title_red">
                                Summary of Key Features:
                            </div>
                            <div class="common-best__container-sibling">
                                <?

                                $see_details_button = Registry::get('Controllers\Product')->getLinkAsElement([
                                    'scope'            => 'page',
                                    'scheme'           => $product['PRODUCT_SYSTEM_1_ATTRIBUTE_LIST']['10153']['value'],
                                    'requested_scheme' => 'cardname',

                                    'page_id'             => $order_page_number,
                                    'customer_id'         => $product['MERCHANT_SYSTEM_1_NUMBER'],
                                    'product_id'          => $product['PRODUCT_SYSTEM_1_NUMBER'],
                                    'product_href'        => $product['PRODUCT_PAGE_URL'],
                                    'product_spreadsheet' => $product['SPREADSHEET_NUMBER'],
                                    'product_content'     => "See more details",
                                    "element"             => [
                                        "class" => [
                                            'common-best__see-more',
                                            'common-best__see-more_red'
                                        ]
                                    ]
                                ]);

                                print $see_details_button;

                                ?>
                            </div>
                        </div>

                        <div class="common-best__product-attributes-container">
                            <?
                            foreach ($data_parameters['attributes'] as $attribute)
                            {
                                $title            = $attribute['extra'];
                                $attribute_number = $attribute['value']
                                ?>
                                <div class="common-best__attribute">
                                    <div class="common-best__attribute-name js-attribute-name-selector">
                                        <span><?php print $title ?></span>
                                    </div>
                                    <div class="common-best__attribute-value js-attribute-value-selector">
                                        <span><?php print $product['PRODUCT_SYSTEM_2_ATTRIBUTE_LIST'][$attribute_number]['output']?></span>
                                    </div>
                                </div>
                                <?
                            }
                            ?>
                        </div>

                    </div>
                    <?# таблица с атрибутами #?>

                    <div class="clear"></div>
                </div>

            </div>
            <?
            $printedProducts++;
        }
        ?>
    </div>
<?
