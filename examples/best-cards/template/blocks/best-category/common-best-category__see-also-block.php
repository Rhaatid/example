<?php
?>
<div class="common-best__see-also">
    <div class="common-best__see-also-title">See also Credit Cards for:</div>
    <div class="common-best__see-also-item" onclick="window.location='../cards-for-excellent-credit.php';">
        <div class="common-best__see-also-content common-best__see-also-content_superior">
            <b>750-850</b><br/>
            Credit Score<br/>
            <span>Excellent Credit History</span>
        </div>
    </div>

    <div class="common-best__see-also-item" onclick="window.location='../cards-for-good-credit.php';">
        <div class="common-best__see-also-content common-best__see-also-content_high">
            <b>700-749</b><br/>
            Credit Score<br/>
            <span>Good Credit History</span>
        </div>
    </div>

    <div class="common-best__see-also-item" onclick="window.location='../cards-for-fair-credit.php';">
        <div class="common-best__see-also-content common-best__see-also-content_medium">
            <b>640-699</b><br/>
            Credit Score<br/>
            <span>Fair Credit History</span>
        </div>
    </div>

    <div class="common-best__see-also-item" onclick="window.location='../1018/1018_page_13372_32277.php';">
        <div class="common-best__see-also-content common-best__see-also-content_small">
            <b>350-639</b><br/>
            Credit Score<br/>
            <span>Bad Credit History</span>
        </div>
    </div>
</div>
<?