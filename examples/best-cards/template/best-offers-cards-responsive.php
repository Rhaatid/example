<?

/**
 * @var \Helpers\Formatter $formatter_object
 * @var \Models\Project $project_object
 * @var \Models\Page $page_object
 * @var \Models\Product $product_object
 */
$project_object   = \Registry::get("Models\Project");
$page_object      = \Registry::get("Models\Page");
$product_object   = \Registry::get("Models\Product");
$formatter_object = \Registry::get("Helpers\Formatter");

$project = $project_object->get([
    "project_number" => PROJECT_NUMBER
]);

$page_parameters = $page_object->getPageParametersOnly([
    'project_number' => PROJECT_NUMBER,
    'page_number'    => $order_page_number
]);

$array_replaces  = [
    '{{YEAR}}'   => date("Y"),
    '{{MONTH}}'  => date("F")
];

$page_parameters = recursive_array_replace(
    array_keys($array_replaces),
    array_values($array_replaces),
    $page_parameters
);

$products_data = $product_object->getPageProductsDataByPage([
    'PAGE_NUMBER_LIST' => [$order_page_number]
]);

$products = $formatter_object->formatRawProductDataOptional([
    'products_data' => $products_data,
    'entity'        => [
        'mnemonics',
        'types'
    ]
]);


require_once(LIBS_PATH."vendor/mmucklo/krumo/class.krumo.php");

//krumo($products);


?>

<? require (BLOCKS_PATH . 'head.php'); ?>

<?
use Models\Objects\WebFileObject;
use Controllers\Loaders\WebStyleFileObjectLoader;


$WebStyleFileLoader = new WebStyleFileObjectLoader([
    'root_path_list' => [
        [
            'internal_root_path' => I_ROOT_PATH . 'build/css/',
            'external_root_path' => E_ROOT_PATH . 'build/css/'
        ]
    ]
]);
$WebStyleFileLoader->load([
    new WebFileObject('layout.css'),
    new WebFileObject('template__best-category-previous-load.css'),
]);
$options = [];

if (DEBUG)
{
    $options = ['method' => 'attached'];
}

$WebStyleFileLoader->render($options);

?>

<?
if (strlen($dot)<=0) $dot = '../';
?>
<script>var page_no=<?=$page_no?>; var dot='<?=$dot?>';</script>

</head>
<body itemscope itemtype="http://schema.org/WebPage">
<div class="page">
    <? include BLOCKS_PATH . "base/header.php"; ?>

    <div class="container container_slide best-gas-grid">

        <? #  блок навигации хлебными крошками # ?>
        <?
        Loader::get(BLOCKS_PATH . 'common__breadcrumbs_new.php', [
            'data_breadcrumbs' => $page_parameters['breadcrumbs']['breadcrumbs']
        ]);
        ?>
        <? #  блок навигации хлебными крошками # ?>


        <?# контентная часть #?>

        <div class="best-gas-grid__container">
            <div class="intro-container">

                <?# текстовый блок #?>
                <?
                # условия нужны, пока не почитсят page_info.php и не перенесут все данные оттуда в стс, как только
                # это сделают, то блок с условиями надо будет снести

                if (!empty($page_parameters['introduction_block']['text'][0]['value'])) {
                    $text = $page_parameters['introduction_block']['text'][0]['value'];
                } else {

                    $text = "<h2>".$arr_page['h2']."</h2>".$arr_page['text1'];
                }

                Loader::get(BLOCKS_PATH . 'common-best-category__text-block.php', [
                    'data_text'   => $text,
                ]);

                ?>
                <?# текстовый блок #?>

            </div>
        </div>


        <div class="best-gas-grid__container">

            <?# табы для страницы #?>
            <?
            Loader::get(BLOCKS_PATH . 'common-best-category__tabs-block.php', [
                'data_tabs'          => $page_parameters['tabs_storage']['tabs'],
                'data_products'      => $products[$order_page_number],
                'data_parameters'    => $page_parameters['tabs_storage'],
                'order_page_number'  => $order_page_number,
                'checkedProducts'    => $chProducts,
                'cntProducts'        => $cntProducts,
                'dot'                => $dot
            ]);
            ?>
            <?# табы для страницы #?>


            <?# сноски #?>
            <div class="footnote"></div>
            <div class="common-best__footnotes">
                <?
                foreach ($page_parameters['footnote']['footnote'] as $footnote)
                {
                    print $footnote['value'] ."<br/>";
                }
                ?>
            </div>
            <?# сноски #?>


            <?# соц сети #?>
            <div class="common-best__social">
                <? include CONTENT_PATH . "soc-button.php"; ?>
            </div>
            <?# соц сети #?>

        </div>

        <div class="best-gas-grid__container">
            <div class="common-best__container common-best__container_two-bordered"></div>
            <div class="common-best__content-bottom">

                <?# текстовый блок #?>
                <?
                # условия нужны, пока не почитсят page_info.php и не перенесут все данные оттуда в стс, как только
                # это сделают, то блок с условиями надо будет снести
                if (!empty($page_parameters['bottom_text']['bottom_text'][0]['value'])) {
                    $text = $page_parameters['bottom_text']['bottom_text'][0]['value'];
                } else {
                    $text = $arr_page['text2'];
                }

                Loader::get(BLOCKS_PATH . 'common-best-category__text-block.php', [
                    'data_text'   => $text,
                ]);
                ?>
                <?# текстовый блок #?>


                <?# блок с факами #?>
                <? require_once (BLOCKS_PATH . 'common-best-category__faqs.php'); ?>
                <?# блок с факами #?>

            </div>

            <div class="common-best__content-bottom common-best__content-bottom_side">

                <?# блок подписки #?>
                <?
                Loader::get(BLOCKS_PATH . 'common-best-category__subscribe.php', [
                ]);
                ?>
                <?# блок подписки #?>


                <?# блок видео #?>
                <?

                #$project_parameters - объявляется в base/header.php

                $video_description = $page_parameters['video']['description'][0];
                if (!empty($page_parameters['video']['name'][0]['value'])) {
                    $video_name = $page_parameters['video']['name'][0]['value'];
                } else {
                    $video_name = $arr_page['video'];
                }

                $video_date        = $project_parameters['no_types']['project']['content']['videos'][$video_name][0];

                $date = DateTime::createFromFormat('Y-m-j', $video_date);
                $date_formatted = $date->format('F d, Y');

                Loader::get(BLOCKS_PATH . 'common-best-category__video-block.php', [
                    'video_title'        => $video_description['value'],
                    'video_description'  => $video_description['extra'],
                    'video'              => $video_name,
                    'date'               => $video_date,
                    'date_formatted'     => $date_formatted
                ]);

                ?>
                <?# блок видео #?>


                <?# блок see also. с монетами там и кредитной историей #?>
                <?
                Loader::get(BLOCKS_PATH . 'common-best-category__see-also-block.php', []);
                ?>
                <?# блок see also. с монетами там и кредитной историей #?>

            </div>
        </div>

        <?# контентная часть #?>
    </div>
</div>
<script src="<?php print E_BUNDLES_PATH . "0.common.js?cache=" . substr( md5( file_get_contents( I_BUNDLES_PATH . "0.common.js" ) ), 0, 8 ) ?>"></script>
<script src="/build/bundles/menu__adaptive.js"></script>
<script src="<?php print E_BUNDLES_PATH . "templates__OLD.js"?>"></script>
<script src="/build/bundles/template__best-category.js"></script>
<script src="<?php print E_BUNDLES_PATH . "footer-signin.js"?>"></script>
<?
$loadJS = [
//    'js_min/jquery.cookie.js',
    'js_min/jquery.tooltipster.js',
    //'js_min/compare-crutch.js',
    'js_min/tooltips/other__tips.js',
    'js_min/ga_social_tracking.js',
    'js_min/script-twitter.js',
];
include CONTENT_PATH . "inc.php";
?>
<? include CONTENT_PATH . "foot.php"; ?>

<!--<link type="text/css" href="/fonts/tab-icons/style.css" rel="stylesheet" media="all"/>-->
<link rel="stylesheet" href="build/svg/svg-style.css?<?=md5(time())?>">
<link type="text/css" href="<?I_ROOT_PATH?>build/css/template__best-category.css?<?=md5(time())?>" rel="stylesheet" media="all"/>
<link type="text/css" href="<?I_ROOT_PATH?>build/css/footer.css?<?=md5(time())?>" rel="stylesheet" media="all"/>
<link type="text/css" href="<?I_ROOT_PATH?>build/css/footer-adaptive.css?<?=md5(time())?>" rel="stylesheet" media="all"/>
<link type="text/css" href="<?I_ROOT_PATH?>build/css/tooltipster.css" rel="stylesheet" media="all"/>
<link type="text/css" href="<?I_ROOT_PATH?>build/css/projekktor.style.css" rel="stylesheet" media="all"/>
<link type="text/css" href="<?=SITE_ROOT?>build/css/subscribe.css" rel="stylesheet" media="all"/>
<? include CONTENT_PATH . "script-facebook.php"; ?>
</body>
</html>