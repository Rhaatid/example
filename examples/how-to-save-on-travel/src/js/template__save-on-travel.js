import $ from 'jquery';
import 'jquery.alignheight';

import 'popup_v2';
import 'tooltip';

import FootnoteLoader from 'footnote';

import Statistic from 'statistic';

let footnoteLoader = new FootnoteLoader({
    querySelector: '.footnote',
    includeСamouflaged: true
});

var sectionAlign = function() {
    $(".advertising-container__block .title").alignHeight();
    $(".products-container__block_first .products-container__block-product-description").alignHeight();
    $(".products-container__block_second .products-container__block-product-description").alignHeight();
    $(".products-container__block_third .products-container__block-product-description").alignHeight();
}

var textContainerBlockAlign = function() {
    $(".text-container__block").alignHeight();
}

// align-height__ghost - класс для выравнивания контейнеров атрибутов, находящихся на одной строке
function alignTwoBlocks() {
    var arr=['.products-container__block-product-name', '.value-current_0', '.value-current_1'];
    $.each(arr, function(index,value){
        var i=0;
        while(i<$(value).length)
        {
            if (i & 1){
                $(value).eq(i-1).addClass('align-height__ghost');
                $(value).eq(i).addClass('align-height__ghost');
                $('.align-height__ghost').alignHeight();
                $('.align-height__ghost').removeClass('align-height__ghost');
            }
            i++;
        }
    })
}

$(document).ready(() => {

    $('.js-tooltip').each(function() {
        var tooltipContent = $(this).attr('data-tooltipster-content');
        $(this).tooltipster({
            contentAsHTML: true,
            position: 'top',
            theme: 'tooltipster-cl',
            content: tooltipContent,
            multiple: true
        });
    });

    $('.js-tooltip-click').each(function() {
        var tooltipContent = $(this).attr('data-tooltipster-content');
        $(this).tooltipster({
            contentAsHTML: true,
            position: 'top',
            theme: 'tooltipster-cl',
            content: tooltipContent,
            multiple: true,
            interactive: true,
            trigger: 'click',
            functionReady: function () {
                $('.js-tooltip-close').on('click', function() {
                    $('.tooltipster-base').detach();
                });
            }
        });
    });

    if( $(window).width() > 0 ) {
        sectionAlign();
        textContainerBlockAlign();
        alignTwoBlocks();
        footnoteLoader.init();
    }
    if ( $(window).width() <= 543 ) {
        $(".products-container__block-product-description-value").css('height', 'auto');
        $(".products-container__block-product-name").css('height', 'auto');
    }
    if ( $(window).width() <= 475 ) {
        $(".text-container__block").css('height', 'auto');
    }
	
	Statistic.updateScreenResolution();
});

$(window).resize(function() {
    if( $(window).width() > 543 ) {
        setTimeout(sectionAlign, 500);
        setTimeout(alignTwoBlocks, 500);
    } else {
        $(".products-container__block-product-description-value").css('height', 'auto');
        $(".products-container__block-product-name").css('height', 'auto');
        $(".products-container__block-product-description").css('height', 'auto');               
    }
    if ( $(window).width() <= 475 ) {
        $(".text-container__block").css('height', 'auto');
    } else {
        textContainerBlockAlign();
    }
});

