<?
use Models\Objects\WebFileObject;
use Controllers\Loaders\WebStyleFileObjectLoader;

$Mustache = new Mustache_Engine;
$products = Registry::get("Models\Product")->getDataUsingPageId([
    "page_number" => ($secondOrder && $fromPage > 0) ? $fromPage : $page_no,
    "index_as"    => "product_number",
    "structure"   => "cts1&cts2"
]);
$cts_parameters = Registry::get("Helpers\Formatter")->formatParametrsCTS2(['array'             => $page_data['template_parameters'],
                                                                           'inserted_is_array' => 1
]);
define("NO_POPUP", true);



?>
<?
require_once(I_BLOCKS_PATH . "base/parameters.php");
require(BLOCKS_PATH . 'head.php');
?>
<?php
    krumo($page_parameters);
    krumo($products);
?>

<!--[if lt IE 8]>
<html lang="en" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html lang="en" class="lt-ie9">        <![endif]-->
<!--[if gt IE 8]>
<html lang="en">                       <![endif]-->

<?

$WebStyleFileLoader = new WebStyleFileObjectLoader([
    'root_path_list' => [
        [
            'internal_root_path' => I_ROOT_PATH . 'build/css/',
            'external_root_path' => E_ROOT_PATH . 'build/css/'
        ]
    ]
]);
$WebStyleFileLoader->load([
    new WebFileObject('template__save-on-travel-previous-load.css'),
    new WebFileObject('header.css'),
    new WebFileObject('layout.css'),
    new WebFileObject('header-adaptive.css')
]);
$options = [];

if (DEBUG)
{
    $options = ['method' => 'attached'];
}

$WebStyleFileLoader->render($options);

?>

</head>
<body itemscope itemtype="http://schema.org/WebPage" data-secondorder="<?= $secondOrder ?>"
      data-frompage="<?= $fromPage ?>">
	 
<div class="page">
    <? include BLOCKS_PATH . "base/header.php"; ?>
    <? # контентная часть #?>
    <div class="travel-grid container_slide">

        <div class="travel-grid__container">
            <? # блок баннера #?>
            <?
            foreach ($products[1] as $product)
            {
                $product_name = Registry::get('Controllers\ProductAttribute')
                                        ->removeSupElementFromName($product["cts1"]["name"]);

                $product_name_link = Registry::get('Controllers\Product')->getLinkAsElement([
                    'scope'            => 'page',
                    'scheme'           => $product["cts1"]['attributes']['10153'],
                    'requested_scheme' => 'cardname',

                    'page_id'             => $page_no,
                    'customer_id'         => $product["cts1"]['customer']['number'],
                    'product_id'          => $product["cts1"]['number'],
                    'product_href'        => $product['cts2']['PRODUCT_PAGE_URL'],
                    'product_content'     => $product_name,
                    'product_spreadsheet' => $product["cts1"]['spreadsheet_number']
                ]);

                $content__image_src = Registry::get('Controllers\Product')->getImageSourceLink([
                    'scale_h'    => 260,
                    'scale_v'    => 260,
                    'shadow'     => 0,
                    'rotation'   => 0,
                    'product_id' => $product['cts1']['number'],
                    'path'       => $product['mnemonics']['image']['value']
                ]);
                $content__image = "<img class=\"product-image-on-banner\" itemprop=\"image\" src=\"" . $content__image_src . "\" alt=\"" . $product["cts2"]['name'] . "\" />";

                $card_image_link = Registry::get('Controllers\Product')->getLinkAsElement([
                    'scope'            => 'page',
                    'scheme'           => $product['cts1']['attributes']['10153'],
                    'requested_scheme' => 'pic',

                    'page_id'         => $page_no,
                    'customer_id'     => $product['cts1']['customer']['number'],
                    'product_id'      => $product['cts1']['number'],
                    'product_href'    => $product['cts2']['PRODUCT_PAGE_URL'],
                    'product_content' => $content__image,
                    'product_spreadsheet' => $product["cts1"]['spreadsheet_number'],

                    'element' => [
                        'class' => [
                            'product-image-on-banner-link'
                        ],
                    ]
                ]);

                $button_link = Registry::get('Controllers\Product')->getLinkAsElement([
                    'scope'            => 'page',
                    'scheme'           => $product["cts1"]['attributes']['10153'],
                    'requested_scheme' => 'button',

                    'page_id'             => $page_no,
                    'customer_id'         => $product["cts1"]['customer']['number'],
                    'product_id'          => $product["cts1"]['number'],
                    'product_href'        => $product['cts2']['PRODUCT_PAGE_URL'],
                    'product_spreadsheet' => $product["cts1"]['spreadsheet_number'],
                    'product_content'     => "Apply Online",
                    "element"             => [
                        "class" => [
                            "button"
                        ]
                    ]
                ]);

                $blocks_text = [
                    $page_parameters["banner"]["banner_text_1"][0],
                    $page_parameters["banner"]["banner_text_2"][0],
                    $page_parameters["banner"]["banner_text_3"][0]
                ];


                Loader::get(BLOCKS_PATH . 'save-on-travel__banner-block.php', [
                    "data_product"     => array_values($products[1])[0],
                    "data_title"       => $page_parameters['banner']['title_banner'][0]['value'],
                    "data_card_name"   => $product_name_link,
                    "data_prd_link"    => $product['cts2']['PRODUCT_PAGE_URL'],
                    "data_image_link"  => $card_image_link,
                    "data_button_link" => $button_link,
                    "banner_img"       => $page_parameters["banner"]["banner_img"][0]['value'],
                    "data_color"       => $page_parameters['redeem']['redeem_icon'][0]['value'],
                    "data_banner_text" => $blocks_text,
                    "data_brands"      => $page_parameters["banner"]["brands"]['values'],
                    "brands_needle"    => $page_parameters["banner"]["brands_need"][0]['values'],
                    "tooltips"         => $page_parameters["banner"]["tooltips"]['values']
                ]);
            }
            ?>
        </div>

        <div class="travel-grid__container-center">
            <? # средняя часть с 3мя блоками и заголовком #?>
            <?
            $button_link = Registry::get('Controllers\Product')->getLinkAsElement([
                'scope'            => 'page',
                'scheme'           => '',
                'requested_scheme' => 'details',

                'page_id'             => $page_no,
                'customer_id'         => $product["cts1"]['customer']['number'],
                'product_id'          => $product["cts1"]['number'],
                'product_href'        => $product['cts2']['PRODUCT_PAGE_URL'],
                'product_spreadsheet' => $product["cts1"][2],
                'product_content'     => "SEE OUR BEST OFFER",
                'element'             => [
                    "class" => [
                        "content-container__button"
                    ]
                ]
            ]);

            $custom = [         # массив, содержащий картинки и alt для блоков
                [
                    'src' => 'img_1.png',
                    'alt' => 'Apply for Travel Credit Card'
                ],
                [
                    'src' => 'img_2.png',
                    'alt' => 'Earn More Miles'
                ],
                [
                    'src' => 'img_3.png',
                    'alt' => 'Get Free Ticket Using Award Miles'
                ]
            ];

            Loader::get(BLOCKS_PATH . 'save-on-travel__3-image-block.php', [
                "data_button_link" => $button_link,
                "data_header"      => $cts_parameters['how_to_get']['h2_get'][0],
                "data_text"        => $cts_parameters['how_to_get']['get_text_array'],
                "custom"           => $custom
            ]);
            ?>
            <? # средняя часть с 3мя блоками и заголовком #?>
        </div>

        <div class="travel-grid__container-products">
            <? # блок с продуктами #?>
            <?
            $content_array = [];
            $data_content = $order_cts_parameters['how_to_earn']['earn_content'];
            for ($i = 0; $i < count($data_content); $i++)
            {
                $array_with_int_keys = array_values($data_content)[$i];
                $array_with_int_keys = array_values($array_with_int_keys);
                $array_with_int_keys1 = array_values($array_with_int_keys);
                $array_with_int_keys2 = array_values($array_with_int_keys1);
                $content_array [] = $array_with_int_keys2;
            }

            Loader::get(BLOCKS_PATH . 'save-on-travel__products-block.php', [
                "data_header"   => $cts_parameters['how_to_earn']['h2_earn'][0],
                "data_content"  => $content_array,
                "page_no"       => $page_no,
                "data_products" => [$products[3], $products[4], $products[5]]
            ]);
            ?>
            <? # блок с продуктами #?>
        </div>

		<?if($cts_parameters["redeem"]["redeem_needle"][0] == '1'){?>
        <div class="travel-grid__container-advertising">
            <? # блок с рекламными предложениями #?>
            <?
            $custom = [         # массив, содержащий картинки и alt для блоков
                [
                    'src' => $cts_parameters["redeem"]["redeem_images"][0],
                    'alt' => 'Apply for Travel Credit Card'
                ],
                [
                    'src' => $cts_parameters["redeem"]["redeem_images"][1],
                    'alt' => 'Earn More Miles'
                ],
                [
                    'src' => $cts_parameters["redeem"]["redeem_images"][2],
                    'alt' => 'Get Free Ticket Using Award Miles'
                ]
            ];

            # подготовка данных, которые пришли из стс, для передачи в блок (обезличиваются ключи - менюятся на int) p.s следите за уровнем вложенности
            $header_array = [];
            $data_header = $cts_parameters['redeem']['redeem_header'];
            for ($i = 0; $i < count($data_header); $i++)
            {
                $array_with_int_keys = array_values($data_header)[$i];
                $header_array [] = $array_with_int_keys;
            }

            $content_array = [];
            $data_content = $cts_parameters['redeem']['redeem_content'];
            for ($i = 0; $i < count($data_content); $i++)
            {
                $array_with_int_keys = array_values($data_content)[$i];
                $array_with_int_keys = array_values($array_with_int_keys);
                $content_array [] = $array_with_int_keys;
            }

            Loader::get(BLOCKS_PATH . 'save-on-travel__advertising-block.php', [
                "data_header"  => $header_array,
                "data_content" => $content_array,
                "custom"       => $custom,
                "data_icon"    => $cts_parameters['redeem']['redeem_icon'],
                "show_popup"    => $cts_parameters['pop_up']['pop_up_availability'][0]
            ]);
            ?>
            <? # блок с рекламными предложениями #?>
        </div>
		<?}?>

        <div class="travel-grid__container-ability">
            <? # блок о возможностях использования #?>
            <?
            if ( $cts_parameters['Ability']['needle'][0] == TRUE ) {
                # подготовка данных, которые пришли из стс, для передачи в блок (обезличиваются ключи - менюятся на int) p.s следите за уровнем вложенности
                $content_array = [];
                $data_content = $cts_parameters['Ability']['ability_content'];
                for ($i = 0; $i < count($data_content); $i++)
                {
                    $array_with_int_keys = array_values($data_content)[$i];
                    $array_with_int_keys = array_values($array_with_int_keys);
                    $content_array [] = $array_with_int_keys;
                }

                Loader::get(BLOCKS_PATH . 'save-on-travel__ability-block.php', [
                    "data_header"  => $cts_parameters['Ability']['h2_ability'][0],
                    "data_content" => $content_array
                ]);
            }
            ?>
            <? # блок о возможностях использования #?>
        </div>

        <div class="travel-grid__container-comments">
            <? # блок комментариев #?>
            <div class="container-comments">
                <?
                foreach ($cts_parameters['footnotes_for_page'] as $value)
                {
                    ?>
                    <p><?php print $value[0] ?></p>
                    <?
                }
                ?>
                <div class="footnote"></div>
            </div>
            <? # блок комментариев #?>
        </div>

        <div class="traver-grid__container-faqs">
            <? # блок факов # ?>
            <?
            if ( $cts_parameters['FAQS']['needle'][0] == TRUE ) {
                $title = $cts_parameters['FAQS']['title'][0];
                include( BLOCKS_PATH . 'save-on-travel__faqs-block.php');
            }
            ?>
            <? # блок факов # ?>
        </div>

        <div class="updated"><b>Updated:</b> <?=date("F j, Y", strtotime("yesterday"))?></div>

        <div class="travel-grid__container-links">
            <? # блок с ссылками #?>
            <div class="container-links">
                <?
                Loader::get(BLOCKS_PATH . 'save-on-travel__links-block.php', [
                    "data_header"        => $cts_parameters['bottom_links']['bottom_links_left']['h3_link_left'][0],
                    "data_links_content" => $cts_parameters['bottom_links']['bottom_links_left']['link_title_left'],
                    "data_links_id"      => $cts_parameters['bottom_links']['bottom_links_left']['page_id_left']
                ]);
                Loader::get(BLOCKS_PATH . 'save-on-travel__links-block.php', [
                    "data_header"        => $cts_parameters['bottom_links']['bottom_links_right']['h3_link_right'][0],
                    "data_links_content" => $cts_parameters['bottom_links']['bottom_links_right']['link_title_right'],
                    "data_links_id"      => $cts_parameters['bottom_links']['bottom_links_right']['page_id_right']
                ]);
                ?>
            </div>
            <? # блок с ссылками #?>
        </div>
		
        <div class="travel-grid__container-discuss">
            <? # блок дискуса #?>
            <?
            # через лоадер не сработает, поэтому подключаю так.
			$data_header = false;
            $data_full_header = $cts_parameters['disqus']['heading'][0];
            $data_content = 'credit-land';
            include_once(BLOCKS_PATH . 'common__discus-block.php');
            ?>
            <? # блок дискуса #?>
        </div>
    </div>

</div>
<? # контентная часть #?>
</div>
<script src="<?php print E_BUNDLES_PATH . "0.common.js?cache=" . substr( md5( file_get_contents( I_BUNDLES_PATH . "0.common.js" ) ), 0, 8 ) ?>"></script>
<script type="text/javascript" src="/build/bundles/menu__adaptive.js"></script>
<script src="/build/bundles/template__save-on-travel.js?<?=md5(time())?>"></script>
<script src="/build/js/header-footer__functions.js"></script>
<script type="text/javascript" src="/js_min/ga_social_tracking.js"></script>
<script type="text/javascript" src="/js_min/script-twitter.js"></script>
<script src="<?php print E_BUNDLES_PATH . "footer-signin.js"?>"></script>
<link rel="stylesheet" href="build/svg/svg-style.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/template__save-on-travel.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/footer.css?<?=md5(time())?>">
<link rel="stylesheet" href="build/css/footer-adaptive.css?<?=md5(time())?>">
<link type="text/css" href="<?=SITE_ROOT?>build/css/subscribe.css" rel="stylesheet" />
<!-- <link rel="stylesheet" href="">   -->
<? include BLOCKS_PATH . "footer.php"; ?>

<? include CONTENT_PATH . "script-facebook.php"; ?>

</body>
</html>
