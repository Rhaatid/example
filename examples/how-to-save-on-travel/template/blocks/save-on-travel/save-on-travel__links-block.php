<div class="container-links__block">
  <div class="title">
    <h3><?=$data_header?></h3>
  </div>
  <ul class="list">
    <?
      #соберём оба массива в один и параллельно вывдем
      $data_links_id_plus_links_content = array_combine($data_links_id, $data_links_content); 
      foreach ($data_links_id_plus_links_content as $key => $value) 
      {
        $page_information = \Registry::get("Models\Page")->getPageRowsByID(array('page_id' => $key));
        ?>
          <li class="list__item">
            <a href="<?php print SITE_ROOT . $page_information[0]['page_url']?>"><?=$value?></a>
          </li>
        <?
      }
    ?>
  </ul>
</div>
<?

?>
