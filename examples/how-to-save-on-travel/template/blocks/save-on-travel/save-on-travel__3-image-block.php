<div class="content-container">
  <div class="content-container__header">
    <h2><?= $data_header?></h2>
    <span>It's easy as 1, 2, 3! </span>
  </div>
  <?
    $custom_plus_parameter_list = array_map( function( $custom_element, $parameter_element ) {
        $custom_element['data'] = $parameter_element;
        return $custom_element;
    
    }, $custom, $data_text );

    $index = 0;
    $class_modifier = "_first";
    foreach ($custom_plus_parameter_list as $value) 
    {
      ?>
        <div class="content-container__block content-container__block<?= $class_modifier ?>">
          <div class="content-container__block-image">
            <img src="./build/images/<?= $custom_plus_parameter_list[$index]['src'] ?>" alt="<?= $custom_plus_parameter_list[$index]['alt'] ?>">
          </div>
          <div class="text">
            <p><?= $custom_plus_parameter_list[$index]['data'][0] ?></p>
          </div>
        </div>
      <?
      $index ++;
      if ($index == 1) { $class_modifier = "_second"; }
      if ($index == 2) { $class_modifier = "_third"; }
    }
  ?>
  <div class="content-container__wrapper-block">
    <?php print $data_button_link ?>
  </div>
</div>
