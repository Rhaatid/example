<div class="banner-container banner-container_<?php print $banner_img ?>" itemscope
     itemtype="http://schema.org/Product">
    <?
    $modified_block = '';

    if ($brands_needle == false)
    {
        $modified_block = 'banner-container__block_full';
    }
    ?>
    <div class="banner-container__block <?php print $modified_block; ?> banner-container__block_<?= $data_color; ?>">
        <div class="banner-container__block-title">
            <?php print $data_title; ?>
        </div>
        <div class="banner-container__block-product brand_circle-<?php print $brand_in_circle?>">
            <div class="banner-container__block-product-name banner-container__block-product-name_<?= $data_color; ?> "
                 itemprop="name">
                <?php print $data_card_name ?>
            </div>
            <link itemprop="url" href="<?= $data_prd_link ?>"/>
            <div class="banner-container__block-product-pic <?= isset($banner_align) ? $banner_align : ""; ?>"
                 itemprop="image">
                <?php print $data_image_link; ?>
                <span class="banner-container__block-product-pic_choice js-tooltip-click"
                      data-tooltipster-content="<span class='js-tooltip-close'></span>This ranking expresses the Credit-Land.com’s experts’ honest opinions, beliefs, and experiences with credit card products offered at Credit-Land.com. While Credit-Land.com receives compensation from most credit card issuers whose offers appear on the site, this ranking is NOT influenced by payment considerations."></span>
            </div>
            <div class="button-container">
                <?php print $data_button_link ?>
            </div>
        </div>
    </div>
    <? #переработать этот момент. выкинуть в отдельный блок, подключать в зависимости от подготовленного параметра в шаблоне
    if ($brands_needle == 1)
    {
        ?>
        <div class="banner-container__block">
            <div class="banner-container__block-brand-align">
                <?
                $brand_number = 0;

                foreach ($data_brands as $brand)
                {
                    if (!empty($tooltips))
                    {
                        $tooltip_text = $tooltips[$brand_number];
                        ?>
                        <div class="banner-container__block-brand js-tooltip"
                             data-tooltipster-content="<?= $tooltip_text ?>">
                            <img class="brand-image"
                                 src="<?php print BUILD_PATH . "images/save-on-travel__brand/" . $brand . ".png" ?>"
                                 alt="<?php print $brand ?>">
                        </div>
                        <?
                        $brand_number++;
                    }
                    else
                    {
                        ?>
                        <div class="banner-container__block-brand ">
                            <img class="brand-image"
                                 src="<?php print BUILD_PATH . "images/save-on-travel__brand/" . $brand . ".png" ?>"
                                 alt="<?php print $brand ?>">
                        </div>
                        <?
                    }
                }
                ?>
            </div>
        </div>
        <?
    }
    ?>
    <div class="travel-grid__container-center">
        <div class="text-container">
            <?
            foreach ($data_banner_text as $block_text)
            {
                //                print $block_text['value'];
                switch ($block_text['type'])
                {
                    case "Product Attribute":
                        $attribute = $block_text['value'];
                        $banner_text = $data_product['cts2']['attributes'][$attribute];
                        break;
                    default:
                        $banner_text = $block_text['value'];
                        break;
                }
                ?>
                <div class="text-container__block" itemprop="description"><p><?php print $banner_text ?></p></div>
                <?
            }
            ?>
        </div>
    </div>
</div>
