<div class="container-faqs">
    <h3><?php print $title?></h3>
    <?
    $filter = array();
    if(isset($cts_parameters['FAQS']['faqs_ids']) && !empty($cts_parameters['FAQS']['faqs_ids'])){
        $field = 'ID';
        foreach ($cts_parameters['FAQS']['faqs_ids'] as $id){
            $filter[] = '='.$id;
        }
    }else{
        $tag_parameter = $cts_parameters['FAQS']['faqs_tag'][0];
        $tag_parameter = "'$tag_parameter'";
        $slug = str_replace(" ", "-",strtolower($tag_parameter));
        $slug = str_replace("'", "", $slug);
        $filter =  array("= '" . $slug . "'");
        $field = 'slug';
    }
    $arr_query = array(
        'wp'     => 'faqs',
        'fields' => array('ID', 'post_name', 'post_title', 'post_date', 'post_content', 'slug'),
        'where'  => array(
            $field        => $filter,
            'post_status' => array("= 'publish'"),
            'post_type'   => array("= 'post'")
        ),
        'where_operator' => array('or'),
        'order'  => array('DESC' => array('post_date')),
        'limit'  => 5
    );

    $query = \HandlerWP::getPosts($arr_query);

    $result = $DBC['media']->query_fetch_all($query);

    if(isset($cts_parameters['FAQS']['faqs_ids']) && !empty($cts_parameters['FAQS']['faqs_ids'])){
        $tmp_res = [];
        foreach ($cts_parameters['FAQS']['faqs_ids'] as $item){
            foreach ($result as $post){
                if ($item == $post['ID']){
                    $tmp_res[] = $post;
                }
            }
        }
        $result = $tmp_res;
        unset($tmp_res);
    }
    for($n = 0; $n < count($result); $n++) {
        ?>
        <div class="faqs-block__question-icon icon-question"></div>
        <div itemscope itemtype="http://schema.org/Question" class="faqs-block__article">
            <a itemprop="about" itemscope itemtype="http://schema.org/Question" class="faqs-block__article-name" href="<?=HandlerWP::getPostHref('faqs', $result[$n])?>">
                <?= isset($_COOKIE['dia']) ? $result[$n]['ID']:""?>
                <?= $result[$n]['post_title']; ?>
            </a>
            <div itemprop="acceptedAnswer" itemscope itemtype="http://schema.org/Answer" class="faqs-block__article-text"><?= trim_text($result[$n]['post_content'], 230);?></div>
            <a class="faqs-block__article-link" href="<?=HandlerWP::getPostHref('faqs', $result[$n])?>">Read more ></a>
        </div>
        <?
    }
    ?>
</div>
