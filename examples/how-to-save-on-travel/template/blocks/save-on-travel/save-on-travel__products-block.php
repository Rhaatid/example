<div class="products-container">
  <div class="products-container__header">
    <h2><?=$data_header?></h2>
  </div>
  <?php
    $block_id = 0;
    foreach ($data_products as $products){
      $block_id++;
      $block_id_ar = $block_id - 1;
      $block_name = "";
      switch ($block_id) {
        case 1: $block_name = "first"; break;
        case 2: $block_name = "second"; break;
        case 3: $block_name = "third"; break;
      }
      ?>
        <div class="products-container__block products-container__block_<?=$block_name?>">
          <div class="products-container__block-description">
            <h3><?=$data_content[$block_id_ar][0]["h3_$block_id"][0]?></h3>
            <p><?=$data_content[$block_id_ar][0]["earn_text_$block_id"][0]?></p>
          </div>
          <?php
            $i = 0; // тут берем номер продукта в блоке(таблице) по счету 0-1
            foreach ($products as $product){?>
              <div class="products-container__block-product" itemscope itemtype="http://schema.org/Product">
                <div class="products-container__block-product-name" itemprop="name">
                <?php
                $product_name = Registry::get('Controllers\ProductAttribute')->removeSupElementFromName(
                    $product["cts1"]["name"]
                );
                print Registry::get('Controllers\Product')->getLinkAsElement(array(
                    'scope' => 'page',
                    'scheme' => $product["cts1"]['attributes']['10153'],
                    'requested_scheme' => 'cardname',

                    'page_id'         => $page_no,
                    'customer_id'     => $product["cts1"]['customer']['number'],
                    'product_id'      => $product["cts1"]['number'],
                    'product_href'    => $product["cts1"]['page']['url'],
                    'product_content' => $product_name,
                    'product_spreadsheet'   => $product["cts1"]['spreadsheet_number']
                ));
                ?>
              </div>
              <link itemprop="url" href="<?=$product["cts1"]['page']['url']?>"/>
              <div class="products-container__block-product-image products-container__block-product-image_<?=$data_content[$block_id_ar][1]["icon_earn_block_$block_id"][$i]?>" >
              <?
              $product_image_src = Registry::get("Controllers\Product")->getImageSourceLink(array(
                  "product_id" => $product["cts1"]['number'],
                  "scale_h"    => 160,
                  "scale_v"    => 160,
                  "path"       => $product['mnemonics']['image']['value']
              ));
              $product_image = "<img src=\"{$product_image_src}\" alt=\"".strip_tags($product["cts1"]['customer']['name']) .": ". $product["cts1"]['name'] . "\"  class=\"".getOrientationCard($product["cts1"]['number'])."\" itemprop=\"image\" />";
              print Registry::get('Controllers\Product')->getLinkAsElement(array(
                  'scope' => 'page',
                  'scheme' => $product["cts1"]['attributes']['10153'],
                  'requested_scheme' => 'pic',
                  'page_id'         => $page_no,
                  'customer_id'     => $product["cts1"]['customer']['number'],
                  'product_id'      => $product["cts1"]['number'],
                  'product_href'    => $product["cts1"]['page']['url'],
                  'product_content' => $product_image,
                  'product_spreadsheet'   => $product["cts1"]['spreadsheet_number']
              ));
              ?>
              </div>
              <div class="products-container__block-product-description">
                <?php
                  $item_num = 0;
                  foreach ($data_content[$block_id_ar][1]["title_for_attributes_$block_id"] as $item){?>
                <div class="products-container__block-product-itemprop-description" itemprop="description" >
                  <div class="products-container__block-product-description-name"><?=$item?></div>
                  <div class="products-container__block-product-description-value value-current_<?=$item_num?>">
                    <?php
                      $val = preg_replace('/^ID/','', $data_content[$block_id_ar][1]["attributes_card_$block_id"][$item_num]);
                      //print $val;
                      print $product["cts2"]['attributes'][$val];
                    ?>
                  </div>
                </div>
                    <?php if ($item_num!= count($data_content[$block_id_ar][1]["title_for_attributes_$block_id"])-1){?>
                <div class="products-container__block-product-description-lace"></div>
                      <?php } ?>
                <?php
                    $item_num++;
                  }
                ?>
              </div>

                <?php
                print Registry::get('Controllers\Product')->getLinkAsElement(array(
                    'scope' => 'page',
                    'scheme' => $product["cts1"]['attributes']['10153'],
                    'requested_scheme' => 'button',

                    'page_id'         => $page_no,
                    'customer_id'     => $product["cts1"]['customer']['number'],
                    'product_id'      => $product["cts1"]['number'],
                    'product_href'    => $product["cts1"]['page']['url'],
                    'product_spreadsheet'   =>  $product["cts1"]['spreadsheet_number'],
                    'product_content' => "Apply Online",
                    "element" => array(
                        "class" => array(
                            "products-container__block-product-button"
                        )
                    )
                ));
                ?>

            </div>
            <? 
              if ($i < count($products)-1) 
              {
                ?>
                  <div class="products-container__block_separator"></div>
                <?
              }
            ?>
          <?php
              $i++;
            }?>
        </div>
      <?php } ?>
</div>
