<div class="ability-container">
  <div class="ability-container__header">
    <h2><?php print $data_header?></h2>
  </div>
<?
$i =0;
  foreach ($data_content as $block) {
    ?>
      <div class="ability-container__block">
        <div class="ability-container__block-image"><img src="<?php print BUILD_PATH ."images/". $block[0][0] .".png"?>" alt="<?php print $block[1][0]?>"></div>
        <div class="ability-container__block-title"><?php print $block[1][0]?></div>
      </div>
    <?
  }
?>
</div>
