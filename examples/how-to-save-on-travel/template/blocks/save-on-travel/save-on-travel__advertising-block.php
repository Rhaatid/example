<?php
    if(!isset($show_popup)) $show_popup = 1;
?>
<div class="advertising-container">
  <div class="advertising-container__header">
    <h2><?= $data_header[0][0] ?></h2>
    <span><?= $data_header[1][0] ?></span>
  </div>
  <?
    unset($index);
    for ( $i = 0; $i < count ( $data_content ); $i++) 
    {
      ?>
        <div class="advertising-container__block">
          <div class="advertising-container__block-offer">
            <div class="title"><span class="text"><?php print $data_content[$i][0][0] ?></span></div>
            <div class="image <?= $show_popup ? "popup-element-".$i :"no-active"?>"><img src="<?= BUILD_PATH . "images/" .$custom[$i]['src']?>" alt="<?=$custom[$i]['alt']?>"></div>
            <div class="description">
              <?= $data_content[$i][1][0] ?>
              <p><?= $data_content[$i][2][0]?></p>
            </div>
            <button class="popup-call_<?=$data_icon[0]?> <?= $show_popup ? "popup-element-".$i:"no-active"?>">START EARNING</button>
          </div> 
        </div>
      <?
    }
  ?>
</div>
